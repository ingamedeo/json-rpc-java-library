package group_2.broker;

import group_2.broker.database.ClientDao;
import group_2.broker.database.ServiceDao;
import group_2.broker.managers.ClientMessageManager;
import group_2.broker.managers.ServerMessageManager;
import group_2.library.Pair;
import group_2.library.channels.AbstractChannelFactory;
import group_2.library.channels.ListenChannel;
import group_2.library.channels.SendChannel;
import group_2.library.entities.Id;
import group_2.library.facades.JRPCClient;
import group_2.library.facades.JRPCServer;
import group_2.library.facades.Library;
import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;

import static org.junit.Assert.*;

public class MainTest {

    private String messageToBrokerListen = null;
    private String messageFromBrokerListen = null;
    private int sender = -1;
    private int msgTo = -1;

    @Before
    public void setUp() throws Exception {

        //Broker start!
        ServiceDao.getInstance().deleteAllServices();
        ClientDao.getInstance().deleteAllClients();

        //Start by creating a channel factory
        AbstractChannelFactory mockFactory = new MockFactory();

        //Initialise JSON-RPC library providing way to use underlying network impl.
        Library.init(mockFactory);

        //Register new JSON-RPC server
        JRPCServer jrpcServer = Library.getServer();
        jrpcServer.registerServer("0", 0, ClientMessageManager.getInstance());

        //Register new JSON-RPC client
        JRPCClient jrpcClient = Library.getClient();
        jrpcClient.registerForServerMessages(ServerMessageManager.getInstance());
    }

    @Test
    public void main() throws Exception {

        //Start with the service registration test
        send(0, "{\"jsonrpc\":\"2.0\",\"id\":1,\"method\":\"registerService\",\"params\":{\"identifier\":\"hello\",\"owner\":\"Amedeo\",\"title\":\"somma\",\"description\":\"ajejebrazorf\",\"sector\":\"math\",\"keywords\":[\"somma\",\"sommaxy\"],\"inputformat\":\"[num, num]\",\"outputformat\":\"{result: num}\"}}");
        assertEquals("{\"jsonrpc\":\"2.0\",\"id\":1,\"result\":\"Service registered\"}", receive());
        System.out.println("");

        //Register another service with another client
        send(1, "{\"jsonrpc\":\"2.0\",\"id\":8,\"method\":\"registerService\",\"params\":{\"identifier\":\"subtract\",\"owner\":\"Tommaso\",\"title\":\"subtract\",\"description\":\"cool service\",\"sector\":\"math\",\"keywords\":[\"subtract\"],\"inputformat\":\"[num, num]\",\"outputformat\":\"{result: num}\"}}");
        assertEquals("{\"jsonrpc\":\"2.0\",\"id\":8,\"result\":\"Service registered\"}", receive());
        System.out.println("");

        //Search the service, ignoring the activation date
        send(0, "{\"jsonrpc\":\"2.0\",\"id\":10,\"method\":\"searchService\",\"params\":{\"search_keywords\":[\"Amedeo\"]}}");
        String recv = receive();
        int index = recv.indexOf("\"activationdate\":\"")+"\"activationdate\":\"".length();
        String firstPart = recv.substring(0, index); //219
        assertEquals("{\"jsonrpc\":\"2.0\",\"id\":10,\"result\":[{\"uniqueServiceIdentifier\":\"hello0\",\"identifier\":\"hello\",\"title\":\"somma\",\"owner\":\"Amedeo\",\"description\":\"ajejebrazorf\",\"sector\":\"math\",\"keywords\":[\"somma\",\"sommaxy\"],\"activationdate\":\"",
                firstPart);
        String secondPart = recv.substring(index+19);
        assertEquals("\",\"inputformat\":\"[num, num]\",\"outputformat\":\"{result: num}\"}]}", secondPart);
        System.out.println("");

        //Try to delete a non-registered  service
        send(0, "{\"jsonrpc\":\"2.0\",\"id\":\"5\",\"method\":\"deleteService\",\"params\":{\"identifier\":\"none\"}}");
        assertEquals("{\"jsonrpc\":\"2.0\",\"id\":\"5\",\"error\":{\"code\":-32000,\"message\":\"Server error\",\"data\":\"This service does not exist.\"}}", receive());
        System.out.println("");

        //Try to delete a service registered by another client
        send(0, "{\"jsonrpc\":\"2.0\",\"id\":11,\"method\":\"deleteService\",\"params\":{\"identifier\":\"subtract\"}}");
        assertEquals("{\"jsonrpc\":\"2.0\",\"id\":11,\"error\":{\"code\":-32000,\"message\":\"Server error\",\"data\":\"This service does not exist.\"}}", receive());
        System.out.println("");

        //Delete the service
        send(0, "{\"jsonrpc\":\"2.0\",\"id\":\"test\",\"method\":\"deleteService\",\"params\":{\"identifier\":\"hello\"}}");
        assertEquals("{\"jsonrpc\":\"2.0\",\"id\":\"test\",\"result\":\"deleted\"}", receive());
        System.out.println("");

        //Search the service again, should not return any service
        send(0, "{\"jsonrpc\":\"2.0\",\"id\":2,\"method\":\"searchService\",\"params\":{\"search_keywords\":[\"somma\"]}}");
        assertEquals("{\"jsonrpc\":\"2.0\",\"id\":2,\"result\":[]}", receive());
        System.out.println("");

        //Request the "subtract1" service
        send(2, "{\"jsonrpc\":\"2.0\",\"id\":88,\"method\":\"subtract1\",\"params\":[3,1]}");
        assertEquals("{\"jsonrpc\":\"2.0\",\"id\":0,\"method\":\"subtract\",\"params\":[3,1]}", receive());
        assertEquals(1, msgTo);//Check that the request was forwarded to the correct client
        System.out.println("");

        //Request the "subtract1" service again from a different client
        send(3, "{\"jsonrpc\":\"2.0\",\"id\":99,\"method\":\"subtract1\",\"params\":[3,1]}");
        assertEquals("{\"jsonrpc\":\"2.0\",\"id\":1,\"method\":\"subtract\",\"params\":[3,1]}", receive());
        assertEquals(1, msgTo);
        System.out.println("");

        //Answer to the second request and check that the response is forwarded
        send(1, "{\"jsonrpc\":\"2.0\",\"id\":1,\"result\":2}");
        assertEquals("{\"jsonrpc\":\"2.0\",\"id\":99,\"result\":2}", receive());
        assertEquals(3, msgTo);
        System.out.println("");

        //send a notification and check that it is forwarded
        send(4, "{\"jsonrpc\":\"2.0\",\"method\":\"subtract1\",\"params\":[22]}");
        assertEquals("{\"jsonrpc\":\"2.0\",\"method\":\"subtract\",\"params\":[22]}", receive());
        assertEquals(1, msgTo);
        System.out.println("");

        //check that a request to a non-existent service does produce an error
        send(4, "{\"jsonrpc\":\"2.0\",\"id\":888,\"method\":\"none\",\"params\":[22]}");
        assertEquals("{\"jsonrpc\":\"2.0\",\"id\":888,\"error\":{\"code\":-32601,\"message\":\"Method not found\"}}", receive());
        System.out.println("");

        //Disconnect the client 1 (notification)
        send(1, "{\"jsonrpc\":\"2.0\", \"method\":\"disconnect\"}");
        Thread.sleep(500); //give time to disconnect since no answer is provided
        System.out.println("");

        //Check that the pending request has been answered with the correct error
        assertEquals("{\"jsonrpc\":\"2.0\",\"id\":null,\"error\":{\"code\":-32601,\"message\":\"Method not found\"}}", receive());
        System.out.println("");

        //Search the service again, should not return any service
        send(0, "{\"jsonrpc\":\"2.0\",\"id\":50,\"method\":\"searchService\",\"params\":{\"search_keywords\":[\"subtract\"]}}");
        assertEquals("{\"jsonrpc\":\"2.0\",\"id\":50,\"result\":[]}", receive());
        System.out.println("");



        System.out.println("");

    }

    private void send(int send, String message) {
        sender = send;
        messageToBrokerListen = message;
    }

    private String receive() {
        while(true) {
            if(messageFromBrokerListen != null) {
                String s = messageFromBrokerListen;
                messageFromBrokerListen = null;
                return s;
            } else {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //Not used
    private class MockSendChannel implements SendChannel {

        @Override
        public void connect(String ip, int port) {

        }

        @Override
        public boolean send(String message) {
            return false;
        }

        @Override
        public String receive() {
            return null;
        }

        @Override
        public void close() {

        }
    }

    public class MockListenChannel implements ListenChannel {

        @Override
        public void bind(String ip, int port) {
        }

        @Override
        public void close() {
        }

        @Override
        public Pair<Id, String> receive() {

            while(true) {
                if(messageToBrokerListen != null) {
                    try {
                        String s = messageToBrokerListen;
                        messageToBrokerListen = null;
                        return new Pair<>(new Id(String.valueOf(sender).getBytes("utf-8")), s);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        @Override
        public boolean send(Id id, String message) {
            messageFromBrokerListen = message;
            msgTo= Integer.parseInt(new String(id.getIdValue()));
            return true;
        }
    }



    public class MockFactory extends AbstractChannelFactory {

        @Override
        public ListenChannel createListenChannel() {
            return new MockListenChannel();
        }

        @Override
        public SendChannel createSendChannel() {
            return new MockSendChannel();
        }
    }

}