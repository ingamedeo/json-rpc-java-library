package group_2.broker;

import group_2.library.entities.ErrorObject;
import group_2.library.entities.Value;

public class Constants {

    public final static String UNIQUE_SERVICE_IDENTIFIER = "uniqueServiceIdentifier";
    public final static String CLIENT_ID = "clientId";
    public final static String CLIENT_ID_INTERNAL = "internalClientId";

    public final static String IP = "ip";
    public final static String PORT = "port";

    public final static String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";
    public final static String TIMEZONE = "GMT";

    public final static String IDENTIFIER = "identifier";
    public final static String OWNER = "owner";
    public final static String TITLE = "title";
    public final static String DESCRIPTION = "description";
    public final static String SECTOR = "sector";
    public final static String INPUT_FORMAT = "inputformat";
    public final static String OUTPUT_FORMAT = "outputformat";
    public final static String KEYWORDS = "keywords";
    public final static String ACTIVATION_DATE = "activationdate";

    public final static String SEARCH_KEYWORDS = "search_keywords";

    public static final ErrorObject SERVICE_DOES_NOT_EXIST = new ErrorObject(-32000, "Server error", new Value("This service does not exist."));


}
