package group_2.broker.managers;

import group_2.library.callbacks.ServerMessageCallback;
import group_2.library.entities.ClientMessage;
import group_2.library.entities.Id;
import group_2.library.entities.ServerMessage;
import group_2.library.facades.Library;

/*
*
*
 */
public class ServerMessageManager implements ServerMessageCallback {

    private static ServerMessageManager instance = null;

    private ServerMessageManager() {}

    public static synchronized ServerMessageManager getInstance() {
        if(instance == null) {
            instance = new ServerMessageManager();
        }
        return instance;
    }

    @Override
    public void onServerMessageReceived(ServerMessage serverMessage, Id id, String ip, int port) {
        //A new serverMessage has been received. Just send it back to the intended recipient
        Number reqId = (Number)serverMessage.getId(); //Cast is safe because the request was forwarded with a numeric id

        if(reqId == null) { //Guard method, return immediately
            return;
        }

        ClientMessage replyToMsg = PendingRequestManager.getInstance().getPendingRequest(reqId.intValue());
        ServerMessage replyMsg = null;
        if(serverMessage.getResult() != null && replyToMsg!=null) {
            replyMsg = new ServerMessage(replyToMsg, serverMessage.getResult());
        } else if (serverMessage.getError() != null && replyToMsg!=null) {
            replyMsg = new ServerMessage(replyToMsg, serverMessage.getError());
        } else {
            return;
        }

        Library.getServer().reply(replyMsg, replyToMsg, PendingRequestManager.getInstance().isBatch(reqId.intValue()));
        PendingRequestManager.getInstance().deletePendingRequest(reqId.intValue());
    }

    @Override
    public void onServerBatchMessageReceived(ServerMessage[] serverBatchMessage, Id id, String ip, int port) {
        //Print the received messages; our implementation of the broker should not receive this kind of messages
        for (ServerMessage message : serverBatchMessage) {

            if (message==null) {
                System.out.println("NULL");
            } else {
                System.out.println(message.toString());
            }
        }
    }
}
