package group_2.broker.managers;

import group_2.broker.handlers.*;
import group_2.library.callbacks.ClientMessageCallback;
import group_2.library.entities.*;
import group_2.library.facades.Library;

/*
 * This class is responsible for handling ClientMessages received by the broker.
 * It implements the corresponding library interface, and provides methods to process ClientMessages depending on their content.
 *
 */
public class ClientMessageManager implements ClientMessageCallback {

    private static ClientMessageManager instance = null;

    private ClientMessageManager() {}

    public static synchronized  ClientMessageManager getInstance() {
        if(instance == null) {
            instance = new ClientMessageManager();
        }
        return instance;
    }

    //Callback returns instantly, as required
    @Override
    public void onClientMessageReceived(ClientMessage clientMessage, Id id, String ip, int port) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                ServerMessage message = processMessage(clientMessage, id, ip, port, false);
                if (message!=null) {
                    Library.getServer().reply(message, clientMessage, false);
                }
            }
        }).start();
    }

    //Callback returns instantly, as required
    @Override
    public void onClientBatchMessageReceived(ClientMessage[] clientBatchMessage, Id id, String ip, int port) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                //The messages are processed in the same order they arrive
                for(ClientMessage message: clientBatchMessage) {
                    ServerMessage replyMessage = processMessage(message, id, ip, port, true);
                    if (replyMessage!=null) {
                        Library.getServer().reply(replyMessage, message, true);
                    }
                }
            }
        }).start();

    }

    private ServerMessage processMessage(ClientMessage clientMessage, Id id, String ip, int port, boolean batch) {

        Handler searchHandler = new SearchHandler();
        Handler registrationHandler = new RegistrationHandler();
        Handler deletionHandler = new DeletionHandler();
        Handler disconnectionHandler = new DisconnectionHandler();
        Handler remoteRequestHandler = new RemoteRequestHandler();

        searchHandler.setSuccessor(registrationHandler);
        registrationHandler.setSuccessor(deletionHandler);
        deletionHandler.setSuccessor(disconnectionHandler);
        disconnectionHandler.setSuccessor(remoteRequestHandler);

        ServerMessage replyMsg = searchHandler.handleClientMessage(clientMessage, id, ip, port, batch);

        return replyMsg;
    }








}
