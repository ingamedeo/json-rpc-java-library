package group_2.broker.managers;

import group_2.broker.database.ClientDao;
import group_2.broker.entities.Client;
import group_2.library.entities.Id;

/*
 * This manager uses the DAO object to store and retrieve Clients' details.
 * It may implement additional logic before calling the DAO.
 */

public class ClientManager {

    private static ClientManager instance = null;

    private ClientManager() {}

    public static synchronized ClientManager getInstance() {
        if(instance == null) {
            instance = new ClientManager();
        }
        return instance;
    }

    public void addClient(Client client) {
        ClientDao.getInstance().saveClient(client);
    }

    public void deleteClient(Id clientId){
        ClientDao.getInstance().deleteClient(clientId);
    }

    public Client getClientById(Id id){
        return ClientDao.getInstance().getById(id);
    }
}
