package group_2.broker.managers;

import group_2.library.entities.ClientMessage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
*This class stores pending requests of remote execution which are yet to be served in an inner map.
* It allows the broker to retrieve those given a requestId.
 */

public class PendingRequestManager {

    private static int nextReqId = 0;
    private static PendingRequestManager instance = null;

    private Map<Integer, InternalRequest> requestMap = null;

    private PendingRequestManager() {
        requestMap = new HashMap<>();
    }

    public static synchronized PendingRequestManager getInstance() {
        if(instance == null) {
            instance = new PendingRequestManager();
        }
        return instance;
    }

    public synchronized int insertPendingRequest(ClientMessage clientMessage, boolean batch, String uniqueServiceIdentifier) {
        InternalRequest internalRequest = new InternalRequest(batch, clientMessage, uniqueServiceIdentifier);
        Integer id = nextReqId;
        requestMap.put(id, internalRequest);
        nextReqId++;
        return id;
    }

    public ClientMessage getPendingRequest(int reqId) {
        InternalRequest ir = requestMap.get(reqId);
        if(ir == null) {
            return null;
        }
        return ir.getClientMessage();
    }

    public Boolean isBatch(int reqId) {
        InternalRequest ir = requestMap.get(reqId);
        if (ir == null) {
            return null;
        }
        return ir.isBatch();
    }

    public void deletePendingRequest(Integer uniqueReqId) {
        requestMap.remove(uniqueReqId);
    }

    public List<Integer> getPendingRequestsByServiceIdentifier(String uniqueServiceIdentifier) {
        List<Integer> result = new ArrayList<>();
        for(int i : requestMap.keySet()) {
            if(requestMap.get(i).getUniqueServiceIdentifier().equals(uniqueServiceIdentifier)) {
                result.add(i);
            }
        }
        return result;
    }

    private class InternalRequest {
        private boolean batch = false;
        private String uniqueServiceIdentifier = null;
        private ClientMessage clientMessage = null;

        InternalRequest(boolean batch, ClientMessage clientMessage, String uniqueServiceIdentifier) {
            this.batch= batch;
            this.clientMessage = clientMessage;
            this.uniqueServiceIdentifier = uniqueServiceIdentifier;
        }

        boolean isBatch() {
            return batch;
        }

        String getUniqueServiceIdentifier() {
            return uniqueServiceIdentifier;
        }

        ClientMessage getClientMessage() {
            return clientMessage;
        }
    }

}
