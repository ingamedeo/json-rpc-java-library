package group_2.broker.managers;

import group_2.broker.Constants;
import group_2.broker.database.ServiceDao;
import group_2.broker.entities.Service;
import group_2.library.entities.Id;
import group_2.library.entities.Value;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

public class ServiceManager {

    private static ServiceManager instance = null;
    private static int nextUniqueNumber = 0;

    private ServiceManager() {}

    public static synchronized ServiceManager getInstance() {
        if(instance == null) {
            instance = new ServiceManager();
        }
        return instance;
    }

    //Method to register a service in the broker
    //The return value is false if a service with the same uniqueServiceIdentifier already exists
    public boolean registerService(Id clientId, String remoteServiceIdentifier, String owner,
                                   String title, String description, String sector, List<String> keywords,
                                   String inputFormat, String outputFormat) {
        String uniqueServiceIdentifier = remoteServiceIdentifier + nextUniqueNumber;
        nextUniqueNumber++;
        long date = System.currentTimeMillis();
        Service service = new Service(clientId, remoteServiceIdentifier, uniqueServiceIdentifier, owner, title,
                description, sector, keywords, date, inputFormat, outputFormat);
        return ServiceDao.getInstance().saveService(service);
    }

    //The returned String is used to search for the pending requests related to the deleted service
    public String removeService(Id id, String remoteServiceIdentifier) {

        Service s = ServiceDao.getInstance().getServiceByRemoteId(id, remoteServiceIdentifier);
        if(s == null) {
            return null;
        }
        boolean b = ServiceDao.getInstance().deleteService(s.getUniqueServiceIdentifier());
        if(!b) {
            return null;
        }
        return s.getUniqueServiceIdentifier();
    }

    public void deleteService(Service service){
        ServiceDao.getInstance().deleteService(service);
    }

    public List<Service> getServicesByClientId(Id id){
        return ServiceDao.getInstance().getServicesByClientId(id);
    }

    public Service getByUniqueServiceIdentifier(String id){
        return ServiceDao.getInstance().getByUniqueServiceIdentifier(id);
    }

    public Value searchService(String[] keywords) {

        List<Service> found = ServiceDao.getInstance().getServicesByKeywords(keywords);

        Value serviceList = Value.createAsArrayValue();

        for(Service s : found) {
            serviceList.addArrayElement(buildValueFromService(s));
        }

        return serviceList;
    }

    private Value buildValueFromService(Service s){
        Value currentService = new Value();

        Value serviceIdentifier = new Value(s.getUniqueServiceIdentifier());
        currentService.add(Constants.UNIQUE_SERVICE_IDENTIFIER, serviceIdentifier);

        Value identifier = new Value(s.getRemoteServiceIdentifier());
        currentService.add(Constants.IDENTIFIER, identifier);

        Value title = new Value(s.getTitle());
        currentService.add(Constants.TITLE, title);

        Value owner = new Value(s.getOwner());
        currentService.add(Constants.OWNER, owner);

        Value description = new Value(s.getDescription());
        currentService.add(Constants.DESCRIPTION, description);

        Value sector = new Value(s.getSector());
        currentService.add(Constants.SECTOR, sector);

        List<String> keys = s.getKeywords();
        Value keywds = Value.createAsArrayValue();
        for(String word : keys) {
            keywds.addArrayElement(new Value(word));
        }
        currentService.add(Constants.KEYWORDS, keywds);
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        Timestamp time = new Timestamp(s.getActivationDate());
        sdf.setTimeZone(TimeZone.getTimeZone(Constants.TIMEZONE));
        Value date = new Value(sdf.format(time));
        currentService.add(Constants.ACTIVATION_DATE, date);

        Value input = new Value(s.getInputFormat());
        currentService.add(Constants.INPUT_FORMAT, input);

        Value output = new Value(s.getOutputFormat());
        currentService.add(Constants.OUTPUT_FORMAT, output);

        return currentService;
    }
}
