package group_2.broker.channels;

import com.sun.istack.internal.NotNull;
import group_2.library.Pair;
import group_2.library.channels.ListenChannel;
import group_2.library.entities.Id;
import org.zeromq.ZContext;
import org.zeromq.ZFrame;
import org.zeromq.ZMQ;
import org.zeromq.ZMsg;

/*This class contains the user-defined implementation of the SendChannel interface.
 *This may be changed as long as it implements the channel interface provided by the library.
 */
public class zeroMQRouterChannel implements ListenChannel {

    private ZContext zContext = null;
    private ZMQ.Socket listener = null;

    public zeroMQRouterChannel() {
        zContext = new ZContext(1);
        listener = zContext.createSocket(ZMQ.ROUTER);
    }

    @Override
    public void bind(@NotNull String ip, int port) {
        listener.bind("tcp://"+ip+":"+port);
    }

    @Override
    public Pair<Id, String> receive() {
        ZMsg msg = ZMsg.recvMsg(listener);

        ZFrame identity = msg.pop();
        ZFrame separator = msg.pop();
        ZFrame content = msg.pop();
        msg.destroy();

        boolean isRequest = separator.getData().length==0;

        String contentString = null;
        if (isRequest) {
            contentString = new String(content.getData(), ZMQ.CHARSET);
        } else {
            contentString = new String(separator.getData(), ZMQ.CHARSET);
        }

        msg.destroy();

        return new Pair<>(new Id(identity.getData()), contentString);
    }

    @Override
    public boolean send(@NotNull Id id, @NotNull String message) {

        listener.sendMore(id.getIdValue());
        return listener.send(message.getBytes(ZMQ.CHARSET));
    }

    @Override
    public void close() {
        zContext.destroy();
    }

}
