package group_2.broker.channels;

import com.sun.istack.internal.NotNull;
        import group_2.library.channels.SendChannel;
import org.zeromq.*;

/*This class contains the user-defined implementation of the SendChannel interface.
 *This may be changed as long as it implements the channel interface provided by the library.
 */

public class zeroMQDealerChannel implements SendChannel {

    private ZContext zContext = null;
    private ZMQ.Socket socket = null;

    public zeroMQDealerChannel() {
        zContext = new ZContext(1);
        socket = zContext.createSocket(ZMQ.DEALER);
    }

    @Override
    public void connect(@NotNull String ip, int port) {
        socket.connect("tcp://"+ip+":"+port);
    }

    @Override
    public boolean send(@NotNull String message) {
        System.out.println("zeroMQDealerChannel is ready to send: " + message);
        return socket.send(message, 0);
    }

    @Override
    public String receive() {

        String contentString = null;

        ZMsg msg = ZMsg.recvMsg(socket);

        ZFrame frame = msg.pop();
        while (frame != null) {

            //Skip empty frames, they may be transmitted my ROUTER channels when they think they're talking to a REQ channel.
            if (frame.getData().length==0) {
                frame = msg.pop();
                continue;
            }

            contentString = frame.getString(ZMQ.CHARSET);
            break;
        }

        return contentString;
    }

    @Override
    public void close() {
        zContext.destroy();
    }

}
