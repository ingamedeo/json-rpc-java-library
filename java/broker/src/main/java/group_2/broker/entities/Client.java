package group_2.broker.entities;

import group_2.broker.Constants;
import group_2.broker.Utils;
import group_2.library.entities.Id;
import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

//Set index information
@Indices({
        @Index(value = Constants.CLIENT_ID_INTERNAL, type = IndexType.Unique)
})

/*
 * Class used for storing Client details to be saved in the database.
 * Allows for matching the clientId with the ip and port on which it is connected.
 */

public class Client {

    private Id clientId = null;

    //This is automatically updated whenever Id is updated.
    @org.dizitart.no2.objects.Id
    private String internalClientId = null;

    private String ip = null;
    private Integer port = null;

    //Used by database to build object instance from row structure
    private Client() {
    }

    public Client(Id clientId, String ip, int port) {
        this.clientId = clientId;

        //Update internal client id accordingly. Remember to do so every time clientId is changed!
        this.internalClientId = Utils.generateInternalClientId(clientId);

        this.ip = ip;
        this.port = port;
    }

    public Id getClientId() {
        return clientId;
    }

    public String getIp() {
        return ip;
    }

    public Integer getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "Client{" +
                "clientId=" + clientId +
                ", internalClientId='" + internalClientId + '\'' +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                '}';
    }
}
