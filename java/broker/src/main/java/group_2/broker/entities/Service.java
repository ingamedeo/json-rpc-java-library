package group_2.broker.entities;

import com.sun.istack.internal.NotNull;
import group_2.broker.Utils;
import group_2.library.entities.Id;
import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import java.util.List;

//Set index information
@Indices({
        @Index(value = group_2.broker.Constants.UNIQUE_SERVICE_IDENTIFIER, type = IndexType.Unique),
        @Index(value = group_2.broker.Constants.CLIENT_ID_INTERNAL, type = IndexType.NonUnique)
})

//Class used for storing Service details to be saved in the database.

public class Service {

    private Id clientId = null;

    //This is automatically updated whenever Id is updated. Internal DB representation.
    private String internalClientId = null;

    //This is the non-unique name chosen by the client
    private String remoteServiceIdentifier = null;

    //This is the unique name chosen by the broker, derived from the remoteServiceIdentifier by the ServiceManager
    @org.dizitart.no2.objects.Id
    private String uniqueServiceIdentifier = null;

    private String owner = null;
    private String title = null;
    private String description = null;
    private String sector = null;
    private List<String> keywords = null;
    private Long activationDate = null;

    /*
    * Input and output format are strings in plain text;
    * they are a description of input and output structure given to the user
    * */

    private String inputFormat = null; //Params of service calls will follow this format
    private String outputFormat = null; //Result of service calls will follow this format

    //Used by database to build object instance from row structure
    private Service() {
    }

    public Service(@NotNull Id clientId, @NotNull String remoteServiceIdentifier, @NotNull String uniqueServiceIdentifier, String owner,
                   String title, String description, String sector, @NotNull List<String> keywords, @NotNull Long activationDate,
                   String inputFormat, String outputFormat) {
        this.clientId = clientId;

        //Update internal client id accordingly. Remember to do so every time clientId is changed!
        this.internalClientId = Utils.generateInternalClientId(clientId);

        this.remoteServiceIdentifier = remoteServiceIdentifier;
        this.uniqueServiceIdentifier = uniqueServiceIdentifier;
        this.owner = owner;
        this.title = title;
        this.description = description;
        this.sector = sector;
        this.keywords = keywords;
        this.activationDate = activationDate;
        this.inputFormat = inputFormat;
        this.outputFormat = outputFormat;
    }

    public Id getClientId() {
        return clientId;
    }

    public String getRemoteServiceIdentifier() {
        return remoteServiceIdentifier;
    }

    public String getUniqueServiceIdentifier() {
        return uniqueServiceIdentifier;
    }

    public String getOwner() {
        return owner;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getSector() {
        return sector;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public Long getActivationDate() {
        return activationDate;
    }

    public String getInputFormat() {
        return inputFormat;
    }

    public String getOutputFormat() {
        return outputFormat;
    }

    //Used for debugging
    @Override
    public String toString() {
        return  remoteServiceIdentifier + ' ' +
                owner + ' ' +
                title + ' ' +
                description + ' ' +
                sector + ' ' +
                keywords + ' ';
    }
}
