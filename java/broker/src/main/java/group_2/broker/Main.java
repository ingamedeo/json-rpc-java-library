package group_2.broker;

import group_2.broker.managers.ClientMessageManager;
import group_2.broker.managers.ServerMessageManager;
import group_2.library.channels.AbstractChannelFactory;
import group_2.library.facades.JRPCClient;
import group_2.library.facades.JRPCServer;
import group_2.library.facades.Library;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();

        //Default IP and PORT
        if (args.length==0) {
            main.run("*", 5570);
        }

        //Default IP (*), custom port
        if (args.length==1) {
            main.run("*", Integer.valueOf(args[0]));
        }

        if (args.length==2) {
            //Custom IP and PORT
            main.run(args[0], Integer.valueOf(args[1])); //Run main program
        }
    }

    private void run(String ip, Integer port) {

        System.out.println(" *** Broker run() ***");

        //Start be creating a channel factory
        AbstractChannelFactory zeroMQChannelFactory = new ZeroMQChannelFactory();

        //Initialise JSON-RPC library providing way to use underlying network impl.
        Library.init(zeroMQChannelFactory);

        //Register new JSON-RPC server
        JRPCServer jrpcServer = Library.getServer();
        jrpcServer.registerServer(ip, port, ClientMessageManager.getInstance());

        //Register new JSON-RPC client
        JRPCClient jrpcClient = Library.getClient();
        jrpcClient.registerForServerMessages(ServerMessageManager.getInstance());
    }
}