package group_2.broker;

import group_2.broker.channels.zeroMQDealerChannel;
import group_2.broker.channels.zeroMQRouterChannel;
import group_2.library.channels.AbstractChannelFactory;
import group_2.library.channels.ListenChannel;
import group_2.library.channels.SendChannel;

import java.util.Random;

public class ZeroMQChannelFactory extends AbstractChannelFactory {

    private static Random rand = new Random(System.nanoTime());

    public ListenChannel createListenChannel() {
        return new zeroMQRouterChannel();
    }

    public SendChannel createSendChannel() {
        return new zeroMQDealerChannel();
    }
}
