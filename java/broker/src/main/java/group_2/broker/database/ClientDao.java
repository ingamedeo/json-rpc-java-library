package group_2.broker.database;

import com.sun.istack.internal.NotNull;
import group_2.broker.Constants;
import group_2.broker.entities.Client;
import group_2.library.entities.Id;

import java.util.List;

/**
 *
 *     >> Available API <<
 *
 * - boolean saveClient(Client client)
 * - boolean deleteClient(Id id)
 * - boolean deleteClient(Client client)
 * - List<Client> getAllClients()
 * - List<Client> getClientsByIP(String ip)
 * - List<Client> getClientsByPort(Integer port)
 * - Client getById(Id id)
 *
 */


/*
 * This is the Data Access Object; it is an entry point to the database implementation, providing high level APIs to manage Client data.
 * The database implementation can easily be replaced; the application continues working as long as this class is adjusted accordingly.
 */
public class ClientDao {

    private static ClientDao instance = null;

    private ClientDao() {}

    public static synchronized ClientDao getInstance() {
        if(instance == null) {
            instance = new ClientDao();
        }
        return instance;
    }

    //Synchronized method, avoid simultaneous calls to this method
    public synchronized void saveClient(@NotNull Client client) {

        Client foundClient = getById(client.getClientId());
        if (foundClient == null) {
            Database.getInstance().saveClient(client);
        }
    }

    public synchronized void deleteClient(@NotNull Id id) {

        Client foundClient = getById(id);
        if (foundClient != null) {
            Database.getInstance().deleteClient(foundClient);
        }
    }

    public synchronized void deleteAllClients() {
        Database.getInstance().deleteAllClients();
    }

    public List<Client> getAllClients() {
        return Database.getInstance().getAllClients();
    }

    public List<Client> getClientsByIP(@NotNull String ip) {
        return Database.getInstance().findClientsBy(Constants.IP, ip);
    }

    public Client getById(@NotNull Id id) {
        return Database.getInstance().findClientBy(Constants.CLIENT_ID_INTERNAL, group_2.broker.Utils.generateInternalClientId(id));
    }
}
