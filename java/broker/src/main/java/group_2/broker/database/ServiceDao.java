package group_2.broker.database;

import com.sun.istack.internal.NotNull;
import group_2.broker.Constants;
import group_2.broker.Utils;
import group_2.broker.entities.Service;
import group_2.library.entities.Id;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

/**
 *
 *     >> Available API <<
 *
 * - boolean saveService(Service service)
 * - List<Service> getAllServices()
 * - List<Service> getServicesByKeywords(String keywords)
 * - boolean deleteService(String uniqueServiceIdentifier)
 * - boolean deleteService(Service service)
 * - Service getByUniqueServiceIdentifier(String uniqueServiceIdentifier)
 * - List<Service> getServicesByClientId(Id id)
 * - Service getServiceByRemoteId(Id id, String remoteIdentifier)
 * - boolean deleteServicesByClientId(Id id)
 *
 */

/*
 * This is the Data Access Object; it is an entry point to the database implementation, providing high level APIs to manage Service data.
 * The database implementation can easily be replaced; the application continues working as long as this class is adjusted accordingly.
 */

public class ServiceDao {

    private static ServiceDao instance = null;

    private ServiceDao() {}

    public static synchronized ServiceDao getInstance() {
        if(instance == null) {
            instance = new ServiceDao();
        }
        return instance;
    }

    public List<Service> getAllServices() {
        return Database.getInstance().getAllServices();
    }

    //Synchronized method, avoid simultaneous calls to this method
    public synchronized boolean saveService(@NotNull Service service) {
        List<Service> foundServices = Database.getInstance().findServicesBy(Constants.CLIENT_ID_INTERNAL, Utils.generateInternalClientId(service.getClientId()));

        for(Service s: foundServices) {
            if(s.getRemoteServiceIdentifier().equals(service.getRemoteServiceIdentifier())) {
                return false;
            }
        }

        Database.getInstance().saveService(service);
        return true;
    }

    public List<Service> getServicesByKeywords(@NotNull String[] keyArray) {

        List<String> keys = new ArrayList<>();
        for(String key: keyArray) {
            if(key == null) {
                continue;
            }
            String tmp = key.trim();
            if(!tmp.equals("")) {
                keys.add(tmp);
            }
        }

        List<Service> serviceList = new ArrayList<>();

        List<Service> services = getAllServices();

        //Search is performed in the metadata and is case-insensitive
        for (Service s : services) {
            if(searchInMetadata(s, keys)) {
                serviceList.add(s);
            }
        }


        return serviceList;
    }

    public synchronized boolean deleteService(@NotNull String uniqueServiceIdentifier) {

        Service foundService = Database.getInstance().findServiceBy(Constants.UNIQUE_SERVICE_IDENTIFIER, uniqueServiceIdentifier);
        if (foundService != null) {
            Database.getInstance().deleteService(foundService);
            return true;
        }

        return false;
    }

    public synchronized boolean deleteService(@NotNull Service service) {
            return deleteService(service.getUniqueServiceIdentifier());
    }

    public synchronized void deleteAllServices() {
        Database.getInstance().deleteAllServices();
    }

    public Service getByUniqueServiceIdentifier(@NotNull String uniqueServiceIdentifier) {
        return Database.getInstance().findServiceBy(Constants.UNIQUE_SERVICE_IDENTIFIER, uniqueServiceIdentifier);
    }

    public List<Service> getServicesByClientId(@NotNull Id id) {
        return Database.getInstance().findServicesBy(Constants.CLIENT_ID_INTERNAL, Utils.generateInternalClientId(id));
    }

    public Service getServiceByRemoteId(@NotNull Id id, @NotNull String remoteIdentifier) {

        List<Service> servicesCId = getServicesByClientId(id);

        for (Service scid : servicesCId) {
            if (scid.getRemoteServiceIdentifier().equals(remoteIdentifier)) {
                return scid;
            }
        }

        return null;
    }

    private boolean searchInMetadata(Service s, List<String> keys) {
        for (String key : keys) {
            List<String> searchStrings = extractMetadataForSearch(s);

            for(String str: searchStrings) {
                if (str.toLowerCase().contains(key.toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }

    //This method searches in the metadata. NOTE: not in the service name, just the metadata!
    private List<String> extractMetadataForSearch(Service s) {
        List<String> res = new ArrayList<>();
        res.add(s.getOwner());
        res.add(s.getSector());
        res.add(s.getTitle());
        res.add(s.getDescription());
        res.addAll(s.getKeywords());
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        Timestamp time = new Timestamp(s.getActivationDate());
        sdf.setTimeZone(TimeZone.getTimeZone(Constants.TIMEZONE));
        res.add(sdf.format(time));
        return res;
    }

}
