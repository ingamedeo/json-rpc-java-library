package group_2.broker.database;

import com.sun.istack.internal.NotNull;
import group_2.broker.entities.Client;
import group_2.broker.entities.Service;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.util.List;

/*
 * Database implementation, this is never accessed directly but always through DAOs.
 * Two tables are provided, one for storing Client data and another for Service data.
 * This is a non-relational database, therefore data integrity is assured programmatically.
 */

class Database {

    private static Database instance = null;

    private static final String DB_PATH = "broker.db";

    private Nitrite db = null;
    private ObjectRepository<Service> serviceRepository = null;
    private ObjectRepository<Client> clientRepository = null;

    private Database() {

        db = Nitrite.builder()
                .compressed()
                .filePath(DB_PATH)
                .openOrCreate();

        //Create or open Object repository
        serviceRepository = db.getRepository(Service.class);
        clientRepository = db.getRepository(Client.class);
        }

    static synchronized Database getInstance() {
        if(instance == null) {
            instance = new Database();
        }
        return instance;
    }

    void closeDatabase() {
        db.close();
    }

    /* Services */

    //Save new service
    void saveService(@NotNull Service service) {
        serviceRepository.insert(service);
        db.commit();
    }

    //Delete existing service
    void deleteService(@NotNull Service service) {
        serviceRepository.remove(service);
        db.commit();
    }

    //Delete all services
    void deleteAllServices() {

        List<Service> services = getAllServices();
        for (Service s : services) {
            serviceRepository.remove(s);
        }

        db.commit();
    }

    //Get all services in database
    List<Service> getAllServices() {
        Cursor<Service> services = serviceRepository.find();
        return services.toList();
    }

    //Find services by param
    List<Service> findServicesBy(@NotNull String paramName, Object value) {
        Cursor<Service> services = serviceRepository.find(ObjectFilters.eq(paramName, value));
        return services.toList();
    }

    //Find service by param (This param is assumed to be unique, if this is not the case only the first service found is returned)
    Service findServiceBy(@NotNull String paramName, Object value) {
        return serviceRepository.find(ObjectFilters.eq(paramName, value)).firstOrDefault();
    }

    /* Clients */

    //Save new service
    void saveClient(@NotNull Client client) {
        clientRepository.insert(client);
        db.commit();
    }

    //Delete existing service
    void deleteClient(@NotNull Client client) {
        clientRepository.remove(client);
        db.commit();
    }

    //Delete all clients
    void deleteAllClients() {

        List<Client> clients = getAllClients();
        for (Client c : clients) {
            clientRepository.remove(c);
        }

        db.commit();
    }

    //Get all clients in database
    List<Client> getAllClients() {
        Cursor<Client> clients = clientRepository.find();
        return clients.toList();
    }

    //Find clients by param
    List<Client> findClientsBy(@NotNull String paramName, Comparable value) {
        Cursor<Client> clients = clientRepository.find(ObjectFilters.eq(paramName, value));
        return clients.toList();
    }

    //Find client by param (This param is assumed to be unique, if this is not the case only the first client found is returned)
    Client findClientBy(@NotNull String paramName, Object value) {
        return clientRepository.find(ObjectFilters.eq(paramName, value)).firstOrDefault();
    }
}
