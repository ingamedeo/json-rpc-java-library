package group_2.broker;

import group_2.library.entities.Id;
import group_2.library.entities.Value;


public class Utils {

    public static boolean isValidString(Object string) {

        String s = null;

        if (string==null) {
            return false;
        }

        if (string instanceof String) {
            s = (String) string;
            return s.trim().length()>0;
        }

        return false;
    }

    public static String generateInternalClientId(Id id) {
        if (id!=null) {
            return byteArrayToHex(id.getIdValue());
        }
        return "null";
    }

    private static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a)
            sb.append(String.format("%02x", b));
        return sb.toString();
    }

    //Checks if a primitive value is stored at a certain key
    public static boolean isKeyPrimitive(Value value, String key) {
        if (value.mapHasKey(key) && value.getMapElement(key).isPrimitive()) {
            return true;
        }
        return false;
    }
}
