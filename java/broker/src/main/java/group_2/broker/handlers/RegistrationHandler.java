package group_2.broker.handlers;

import group_2.broker.Constants;
import group_2.broker.Utils;
import group_2.broker.entities.Client;
import group_2.broker.managers.ClientManager;
import group_2.broker.managers.ServiceManager;
import group_2.library.entities.*;

import java.util.ArrayList;
import java.util.List;

/*This class handles the registration of a new service provided by a client.
*It also registers the client in the database upon the first service registration of that client
*/

public class RegistrationHandler extends Handler {

    private static final String REGISTER_METHOD = "registerService";

    @Override
    public ServerMessage handleClientMessage(ClientMessage clientMessage, Id id, String ip, int port, boolean batch) {
        if (!clientMessage.isNotification() && clientMessage.getMethod().equals(REGISTER_METHOD)){
            return registerService(clientMessage, id, ip, port, batch);
        }
        return successor.handleClientMessage(clientMessage,id,ip,port,batch);
    }



    private ServerMessage registerService(ClientMessage message, Id id, String ip, int port, boolean isBatch) {
        //Parse the request
        //Sample request, with the STRING input/output form (saves a json string without any check, no standard format):
        //{"jsonrpc": "2.0", "method": "registerService",
        //      "params": {"identifier": "sommaxy", "owner": "Amedeo", "title": "somma", description": "ajejebrazorf", "sector": "maths",
        //          "keywords": ["somma", "sommaxy"], "inputformat": "[num, num]", "outputformat": "{"result": num}"},
        //      "id": "1"}

        //Params validation
        System.out.println("REGISTER_METHOD");

        Value params = message.getParams();

        if (params==null || !params.isMap()) { //This registerService request doesn't follow the standard above. Let the client know.
            return new ServerMessage(ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
        }

        // We know the format we expect from the client, so we can check against it

        //Check if those keys exist (none can be omitted)
        if (!Utils.isKeyPrimitive(params, Constants.IDENTIFIER) || !Utils.isKeyPrimitive(params, Constants.OWNER) || !Utils.isKeyPrimitive(params, Constants.TITLE) || !Utils.isKeyPrimitive(params, Constants.DESCRIPTION) || !Utils.isKeyPrimitive(params, Constants.SECTOR) || !Utils.isKeyPrimitive(params, Constants.INPUT_FORMAT) || !Utils.isKeyPrimitive(params, Constants.OUTPUT_FORMAT)) {
            return new ServerMessage(ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
        }

        //Getting everything as Object, we will later check if those are valid strings (as we expect) -> isValidString()
        Object identifier = params.getMapElement(Constants.IDENTIFIER).getPrimitive();
        Object owner = params.getMapElement(Constants.OWNER).getPrimitive();
        Object title = params.getMapElement(Constants.TITLE).getPrimitive();
        Object description = params.getMapElement(Constants.DESCRIPTION).getPrimitive();
        Object sector = params.getMapElement(Constants.SECTOR).getPrimitive();
        Object inputFormat = params.getMapElement(Constants.INPUT_FORMAT).getPrimitive();
        Object outputFormat = params.getMapElement(Constants.OUTPUT_FORMAT).getPrimitive();
        Value keywords = params.getMapElement(Constants.KEYWORDS);

        //Keywords is expected to be an array and to be there!
        if (keywords==null || !keywords.isArray()) {
            return new ServerMessage(ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
        }

        List<String> keywordList = new ArrayList<>();

        for (int i = 0; i < keywords.getArraySize(); i++) {
            Value e = keywords.getArrayElement(i);
            if (!e.isPrimitive() || !(e.getPrimitive() instanceof String)) {
                return new ServerMessage(ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
            }

            keywordList.add((String)keywords.getArrayElement(i).getPrimitive());
        }

        if(!Utils.isValidString(identifier) || !Utils.isValidString(owner) || !Utils.isValidString(title) || !Utils.isValidString(description) || !Utils.isValidString(sector)) {
            return new ServerMessage(ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
        }

        // Method names that begin with the word rpc followed by a period character (U+002E or ASCII 46) are reserved for rpc-internal methods and extensions and MUST NOT be used for anything else.
        if (((String)identifier).startsWith("rpc.")) {
            return new ServerMessage(new ErrorObject(-32010, "Method starting with .rpc can't be registered"));
        }

        boolean res = ServiceManager.getInstance().registerService(id, (String)identifier, (String)owner, (String)title, (String)description, (String)sector,
                keywordList, (String)inputFormat, (String)outputFormat);

        if (!res) {
            return new ServerMessage(message, new Value("Service already registered"));
        } else {
            Client client = new Client(id, ip, port);
            ClientManager.getInstance().addClient(client);
            return new ServerMessage(message, new Value("Service registered"));
        }
    }
}
