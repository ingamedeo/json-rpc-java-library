package group_2.broker.handlers;

import group_2.library.entities.ClientMessage;
import group_2.library.entities.Id;
import group_2.library.entities.ServerMessage;

//This class represents an abstract handler, as specified by the "chain of responsibility" pattern
public abstract class Handler {

    protected Handler successor = null;

    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }

    public abstract ServerMessage handleClientMessage(ClientMessage clientMessage, Id id, String ip, int port, boolean batch);
}
