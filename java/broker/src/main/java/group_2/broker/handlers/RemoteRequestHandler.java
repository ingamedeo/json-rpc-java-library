package group_2.broker.handlers;

import group_2.broker.entities.Client;
import group_2.broker.entities.Service;
import group_2.broker.managers.ClientManager;
import group_2.broker.managers.PendingRequestManager;
import group_2.broker.managers.ServiceManager;
import group_2.library.entities.ClientMessage;
import group_2.library.entities.ErrorObject;
import group_2.library.entities.Id;
import group_2.library.entities.ServerMessage;
import group_2.library.facades.Library;

/*This class handles the forwarding of remote requests from one client to the other.
*The remote execution request is received from one client and sent to the one implementing the requested service.
*/
public class RemoteRequestHandler extends Handler{

    @Override
    public ServerMessage handleClientMessage(ClientMessage clientMessage, Id id, String ip, int port, boolean batch) {
        return sendRemoteClientMessage(clientMessage, batch);
    }

    private ServerMessage sendRemoteClientMessage(ClientMessage message, boolean batch) {
        System.out.println("REMOTE_REQUEST");
        //Get the service identifier
        String method = message.getMethod();
        Service searched = ServiceManager.getInstance().getByUniqueServiceIdentifier(method);
        if(searched == null) {

            Object objectId = message.getId();

            if (objectId==null) {
                return new ServerMessage(ErrorObject.createAsStandardError(ErrorObject.ErrorType.METHOD_NOT_FOUND, null));
            } else {
                return new ServerMessage(message, ErrorObject.createAsStandardError(ErrorObject.ErrorType.METHOD_NOT_FOUND, null));
            }

        }

        Integer reqId = null;
        if (!message.isNotification()) {
            reqId = PendingRequestManager.getInstance().insertPendingRequest(message, batch, method);
        }

        //Building new ClientMessage
        ClientMessage.ClientMessageBuilder builder = new ClientMessage.ClientMessageBuilder(searched.getRemoteServiceIdentifier());
        if (reqId != null) {
            builder.id(reqId);
        }

        if (message.getParams()!=null) {
            builder.params(message.getParams());
        }

        ClientMessage forwardMsg = builder.build();

        Id destinationId = searched.getClientId();
        Client client = ClientManager.getInstance().getClientById(destinationId);
        String listenIp = client.getIp();
        Integer listenPort = client.getPort();
        Library.getClient().sendClientMessage(listenIp, listenPort, forwardMsg, destinationId);


        return null; //Means that everything went well
    }

}
