package group_2.broker.handlers;

import group_2.broker.Constants;
import group_2.broker.Utils;
import group_2.broker.entities.Service;
import group_2.broker.managers.PendingRequestManager;
import group_2.broker.managers.ClientManager;
import group_2.broker.managers.ServiceManager;
import group_2.library.entities.*;
import group_2.library.facades.Library;

import java.util.List;


// This class handles the deletion of a service previously registered by the same client.

public class DeletionHandler extends Handler {

    private static final String DELETE_METHOD = "deleteService";


    @Override
    public ServerMessage handleClientMessage(ClientMessage clientMessage, Id id, String ip, int port, boolean batch) {
        if (!clientMessage.isNotification() && clientMessage.getMethod().equals(DELETE_METHOD)){
            System.out.println("DELETE_METHOD");
            return deleteService(clientMessage, id);
        }
        return successor.handleClientMessage(clientMessage, id, ip, port, batch);
    }

    private ServerMessage deleteService(ClientMessage message, Id id) {

        Value params = message.getParams();

        if (params == null || !params.isMap()) { //This registerService request doesn't follow the standard above. Let the client know.
            return new ServerMessage(ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
        }


        if (!Utils.isKeyPrimitive(params, Constants.IDENTIFIER) || !(params.getMapElement(Constants.IDENTIFIER).getPrimitive() instanceof String)) {
            return new ServerMessage(ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
        }

        String identifier = (String)params.getMapElement(Constants.IDENTIFIER).getPrimitive();

        String serviceUniqueId = ServiceManager.getInstance().removeService(id, identifier);

        if (serviceUniqueId == null) {
            return new ServerMessage(message, Constants.SERVICE_DOES_NOT_EXIST);
        }

        //Now respond to the pending requests for this service
        List<Integer> pending = PendingRequestManager.getInstance().getPendingRequestsByServiceIdentifier(serviceUniqueId);
        for(Integer i : pending) {
            Library.getServer().reply(new ServerMessage(ErrorObject.createAsStandardError(ErrorObject.ErrorType.METHOD_NOT_FOUND, null)), PendingRequestManager.getInstance().getPendingRequest(i), PendingRequestManager.getInstance().isBatch(i));
            PendingRequestManager.getInstance().deletePendingRequest(i);
        }


        List<Service> services = ServiceManager.getInstance().getServicesByClientId(id);
        if (services.size()==0) { //There are no services left, remove this client from the DB
            ClientManager.getInstance().deleteClient(id);
        }

        return new ServerMessage(message, new Value("deleted"));
    }

}
