package group_2.broker.handlers;

import group_2.broker.Constants;
import group_2.broker.managers.ServiceManager;
import group_2.library.entities.*;

import java.util.ArrayList;
import java.util.List;
/*
* This class handles search requests from clients connected to the broker.
* Upon receiving a set of keywords, services saved in the broker database are retrieved and those corresponding to the search query are returned.
* The client may then decide to invoke one of them, using its uniqueIdentifier.
*/
public class SearchHandler extends Handler {

    private static final String SEARCH_METHOD = "searchService";

    @Override
    public ServerMessage handleClientMessage(ClientMessage clientMessage, Id id, String ip, int port, boolean batch) {
        if (!clientMessage.isNotification() && clientMessage.getMethod().equals(SEARCH_METHOD)){
            return searchService(clientMessage);
        }
        return successor.handleClientMessage(clientMessage, id, ip, port, batch);
    }

    private ServerMessage searchService(ClientMessage message) {

        //{"jsonrpc": "2.0", "method": "searchService",
        //      "params": {"search_keywords": ["somma", "sommaxy"]},
        //      "id": "1"}

        //extract keywords
        Value params = message.getParams();

        //We only accept parameters in Map format when searching for a service.

        System.out.println("SEARCH_METHOD");

        if (params==null || !params.isMap()) {
            return new ServerMessage(message, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
        }

        //proprietario, settore, parole chiave, titolo, descrizione, data di attivazione.

        if (!params.mapHasKey(Constants.SEARCH_KEYWORDS)) {
            return new ServerMessage(message, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
        }

        Value keywords = params.getMapElement(Constants.SEARCH_KEYWORDS);

        if (!keywords.isArray()){
            return new ServerMessage(message, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
        }
        List<String> keywordsList = new ArrayList<>();
        for (int i = 0; i < keywords.getArraySize(); i++){
            Value current = keywords.getArrayElement(i);
            if (!current.isPrimitive() || !(current.getPrimitive() instanceof String)){
                return new ServerMessage(message, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
            }
            String k = (String) current.getPrimitive();
            keywordsList.add(k);
        }

        //search
        Value replyParams = ServiceManager.getInstance().searchService(keywordsList.toArray(new String[keywordsList.size()]));

        return new ServerMessage(message, replyParams);
    }
}
