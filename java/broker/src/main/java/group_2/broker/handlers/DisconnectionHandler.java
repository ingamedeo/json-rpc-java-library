package group_2.broker.handlers;

import group_2.broker.entities.Service;
import group_2.broker.handlers.Handler;
import group_2.broker.managers.ClientManager;
import group_2.broker.managers.PendingRequestManager;
import group_2.broker.managers.ServiceManager;
import group_2.library.entities.ClientMessage;
import group_2.library.entities.ErrorObject;
import group_2.library.entities.Id;
import group_2.library.entities.ServerMessage;
import group_2.library.facades.Library;

import java.util.List;

//This class handles the disconnection of a client from the broker, simultaneously removing all the services previously registered by a client

public class DisconnectionHandler extends Handler {

    private static final String DISCONNECT_METHOD = "disconnect";


    @Override
    public ServerMessage handleClientMessage(ClientMessage clientMessage, Id id, String ip, int port, boolean batch) {
        if (clientMessage.isNotification() && clientMessage.getMethod().equals(DISCONNECT_METHOD)){
            disconnectClient(id);
            return null;
        }
        return successor.handleClientMessage(clientMessage, id, ip, port, batch);
    }

    private void disconnectClient(Id id) {
        System.out.println("DISCONNECT_METHOD");

        List<Service> serviceList = ServiceManager.getInstance().getServicesByClientId(id);

        for (Service service : serviceList) {

            List<Integer> requestIds = PendingRequestManager.getInstance().getPendingRequestsByServiceIdentifier(service.getUniqueServiceIdentifier());

            for (Integer requestId : requestIds) {

                //Get assoc ClientMessage
                ClientMessage assocClientMessage = PendingRequestManager.getInstance().getPendingRequest(requestId);
                //Reply, the method is no longer available
                Library.getServer().reply(new ServerMessage(ErrorObject.createAsStandardError(ErrorObject.ErrorType.METHOD_NOT_FOUND, null)), assocClientMessage, PendingRequestManager.getInstance().isBatch(requestId));
                //This PendingRequest has been dealt with
                PendingRequestManager.getInstance().deletePendingRequest(requestId);
            }

            ServiceManager.getInstance().deleteService(service);
        }
        ClientManager.getInstance().deleteClient(id);
    }
}
