package group_2.brokerclient.channels;

import group_2.library.Pair;
import group_2.library.channels.ListenChannel;
import group_2.library.entities.Id;
import org.zeromq.ZContext;
import org.zeromq.ZFrame;
import org.zeromq.ZMQ;
import org.zeromq.ZMsg;

public class zeroMQRouterChannel implements ListenChannel {

    private ZContext zContext = null;
    private ZMQ.Socket listener = null;

    public zeroMQRouterChannel() {
        zContext = new ZContext(1);
        listener = zContext.createSocket(ZMQ.ROUTER);
    }

    @Override
    public void bind(String ip, int port) {
        listener.bind("tcp://"+ip+":"+port);
    }

    @Override
    public Pair<Id, String> receive() {
        ZMsg msg = ZMsg.recvMsg(listener);

        ZFrame identity = msg.pop();
        ZFrame separator = msg.pop();
        ZFrame content = msg.pop();
        msg.destroy();

        boolean isRequest = separator.getData().length==0;

        String contentString = null;
        if (isRequest) {
            contentString = new String(content.getData(), ZMQ.CHARSET);
        } else {
            contentString = new String(separator.getData(), ZMQ.CHARSET);
        }

        msg.destroy();

        return new Pair<>(new Id(identity.getData()), contentString);
    }

    @Override
    public boolean send(Id id, String message) {

        listener.sendMore(id.getIdValue());
        return listener.send(message.getBytes(ZMQ.CHARSET));
    }

    @Override
    public void close() {
        zContext.destroy();
    }

}
