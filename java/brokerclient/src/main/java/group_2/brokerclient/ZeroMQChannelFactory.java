package group_2.brokerclient;

import group_2.library.channels.AbstractChannelFactory;
import group_2.library.channels.ListenChannel;
import group_2.library.channels.SendChannel;
import group_2.brokerclient.channels.zeroMQDealerChannel;
import group_2.brokerclient.channels.zeroMQRouterChannel;

import java.util.Random;

public class ZeroMQChannelFactory extends AbstractChannelFactory {

    private static Random rand = new Random(System.nanoTime());

    public ListenChannel createListenChannel() {
        return new zeroMQRouterChannel();
    }

    public SendChannel createSendChannel() {
        //String identity = String.valueOf(rand.nextInt(100));
        return new zeroMQDealerChannel();
    }
}
