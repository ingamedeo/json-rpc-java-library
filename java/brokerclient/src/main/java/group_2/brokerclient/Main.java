package group_2.brokerclient;

import group_2.library.Pair;
import group_2.library.callbacks.ClientMessageCallback;
import group_2.library.callbacks.ServerMessageCallback;
import group_2.library.channels.AbstractChannelFactory;
import group_2.library.entities.*;
import group_2.library.facades.JRPCClient;
import group_2.library.facades.JRPCServer;
import group_2.library.facades.Library;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main implements ClientMessageCallback, ServerMessageCallback {

    /*
        Please note:
        This class does not comply with the single responsibility principle because it is a demo class.
     */

    private static class LocalService {
        private String method;
        private String outputFormat;
        private String inputFormat;

        LocalService(String method, String outputFormat, String inputFormat) {
            this.method = method;
            this.outputFormat = outputFormat;
            this.inputFormat = inputFormat;
        }

        String getMethod() {
            return method;
        }

        String getOutputFormat() {
            return outputFormat;
        }

        String getInputFormat() {
            return inputFormat;
        }
    }

    private static final String IP = "localhost";
    private static final int PORT = 5570;

    private static Scanner scanner = new Scanner(System.in);
    private JRPCClient jrpcClient = Library.getClient();
    private JRPCServer jrpcServer = Library.getServer();
    private boolean callbackCalled =false;
    private static int levelOfRecursion = 0;
    private static int autoIndex = 0;
    private static final LocalService[] localServices = {
            new LocalService("helloWorld", "String", ""),
            new LocalService("sum", "Number", "[Number, Number]"),
            new LocalService("subtract", "Number", "[Number, Number]"),
            new LocalService("randomInteger", "Number", "{\"min\": Number, \"max\": Number}"),
            new LocalService("complexParams", "Number", "{\"min\": {\"array\":[Number, Boolean]}, \"max\": Boolean}")
    };






    public static void main(String[] args) {
        Main main = new Main();
        main.run(); //Run main program
    }

    private void run() {

        //Start be creating a channel factory
        AbstractChannelFactory zeroMQChannelFactory = new ZeroMQChannelFactory();

        //Initialise JSON-RPC library providing way to use underlying network impl.
        Library.init(zeroMQChannelFactory);

        //Register new JSON-RPC server
        jrpcServer.registerForClientMessages(this);

        //Register new JSON-RPC client
        jrpcClient.registerForServerMessages(this);

        while (true) {
            messageChoice();
        }
    }

    private void messageChoice() {
        printlnWithRecursion("");
        printlnWithRecursion("Please choose an option:");
        String[] options = {"Normal message", "Batch message", "Exit"};
        int option = promptForOption(options, false);

        switch (option) {
            case 0: {
                Pair<String, Integer> netparams = selectIPAndPort();
                ClientMessage message = insertMessage();

                try {
                    jrpcClient.sendClientMessage(netparams.getFirst(), netparams.getSecond(), message);
                    waitForResponse();
                } catch (IllegalArgumentException e) {
                    printlnWithRecursion("Invalid JSON-RPC or invalid ip address. Params is primitive, method is missing or dns search failed.");
                }
                break;
            }
            case 1: {
                Pair<String, Integer> netparams = selectIPAndPort();
                List<ClientMessage> messages = new ArrayList<>();
                while(true) {
                    printlnWithRecursion("Please choose an option:");
                    String[] actions = {"Add message to batch", "Send batch"};
                    int action = promptForOption(actions, true);
                    if(action == 0) {
                        ClientMessage message = insertMessage();
                        messages.add(message);
                    } else {
                        break;
                    }
                }
                try {
                    jrpcClient.sendBatchClientMessage(netparams.getFirst(), netparams.getSecond(), messages.toArray(new ClientMessage[messages.size()]));
                    waitForResponse();
                } catch (IllegalArgumentException e) {
                    printlnWithRecursion("Invalid JSON-RPC or invalid ip address. Params is primitive, method is missing or dns search failed.");
                }
                break;
            }
            default: {
                System.exit(0);
                break;
            }

        }
    }

    private ClientMessage insertMessage() {

        printlnWithRecursion("");
        printlnWithRecursion("Please choose an option:");
        String[] options = {"Register new service", "Delete service", "Search service", "Request remote service or send broker API request", "Disconnect", "Exit"};

        int option = promptForOption(options, false);

        switch (option) {
            case 0:
                return createRegisterLocalServiceMessage();
            case 1:
                return createUnregisterLocalServiceMessage();
            case 2:
                return createSearchRemoteServiceMessage();
            case 3:
                return createRemoteExecutionMessage();
            case 4:
                return createDisconnectMessage();
            case 5:
                System.exit(0);
                break;
            default:
                break;
        }
        return null;
    }





    //This method waits for the first callback that is called, or returns after 2 seconds.
    //This synchronizes the reception, allowing the logs to be printed in the correct order.
    private void waitForResponse() {
        printlnWithRecursion("Waiting for response...");
        printlnWithRecursion("");
        int count = 0;
        while(true) {
            if(callbackCalled) {
                callbackCalled = false;
                return;
            } else {
                count++;
                if(count > 20) {
                    printlnWithRecursion("Wait timeout. The message was a notification or it hasn't reached the recipient.");
                    return;
                }
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }






    //Methods for action execution
    private ClientMessage createRegisterLocalServiceMessage() {

        ClientMessage.ClientMessageBuilder builder = new ClientMessage.ClientMessageBuilder("registerService");

        //Get the id
        builder = requestId(builder);

        printlnWithRecursion("The following native methods may be registered:");
        List<String> services = new ArrayList<>();
        for (LocalService localService1 : localServices) {
            services.add(localService1.getOutputFormat() + " " + localService1.getMethod() + "(" + localService1.getInputFormat() + ")");
        }
        int chosenService = promptForOption(services.toArray(new String[services.size()]), false);

        LocalService localService = localServices[chosenService];
        System.out.print("Insert owner: ");
        String owner = scanner.nextLine();
        System.out.print("Insert title: ");
        String title = scanner.nextLine();
        System.out.print("Insert description: ");
        String description = scanner.nextLine();
        System.out.print("Insert sector: ");
        String sector = scanner.nextLine();
        System.out.print("Insert keywords (comma separated) [eg. key1, key2]: ");
        String keywordsStr = scanner.nextLine();
        String[] keywords = keywordsStr.split(",");

        Value regParams = getRegisterParams(localService.getMethod(), owner, title, description, sector, keywords, localService.getInputFormat(), localService.getOutputFormat());

        return builder.params(regParams).build();
    }

    private ClientMessage createUnregisterLocalServiceMessage() {
        ClientMessage.ClientMessageBuilder builder = new ClientMessage.ClientMessageBuilder("deleteService");

        //Get the id
        builder = requestId(builder);

        printlnWithRecursion("The following native methods may be unregistered1:");
        List<String> services = new ArrayList<>();
        for (LocalService localService1 : localServices) {
            services.add(localService1.getOutputFormat() + " " + localService1.getMethod() + "(" + localService1.getInputFormat() + ")");
        }
        int chosenService = promptForOption(services.toArray(new String[services.size()]), false);


        LocalService localService = localServices[chosenService];

        Value deleteParams = new Value("identifier", new Value(localService.getMethod()));
        return builder.params(deleteParams).build();
    }

    private ClientMessage createSearchRemoteServiceMessage() {
        ClientMessage.ClientMessageBuilder builder = new ClientMessage.ClientMessageBuilder("searchService");

        //Get the id
        builder = requestId(builder);

        printlnWithRecursion("Insert the search keywords(comma separated) [eg. key1, key2]: ");
        String searchKeywordsStr = scanner.nextLine();
        String[] searchKeywords = searchKeywordsStr.split(",");
        Value searchParams = Value.createAsArrayValue();
        for (String k : searchKeywords){
            Value element = new Value(k);
            searchParams.addArrayElement(element);
        }
        Value searchData = new Value("search_keywords", searchParams);
        return builder.params(searchData).build();
    }

    private ClientMessage createRemoteExecutionMessage() {

        //Request the method
        printlnWithRecursion("Insert the uniqueServiceIdentifier for the desired service.");
        printlnWithRecursion("Please keep in mind that the broker registers the uniqueServiceIdentifier by adding a number at the end of the actual method name.");
        printlnWithRecursion("Use the search function if unsure.");

        String uniqueServiceIdentifier = scanner.nextLine();

        ClientMessage.ClientMessageBuilder builder = new ClientMessage.ClientMessageBuilder(uniqueServiceIdentifier);

        //Request the id
        builder = requestId(builder);

        Value params = askForParams();
        if (params!=null) {
            builder.params(params);
        }

        return builder.build();

    }
    private ClientMessage createDisconnectMessage() {

        return new ClientMessage.ClientMessageBuilder("disconnect").build();
    }

    //Callback implementation and native methods
    @Override
    public void onClientMessageReceived(ClientMessage clientMessage, Id id, String ip, int port) {
        callbackCalled = true;
        printlnWithRecursion("ClientMessage received. Serving a remote request on this client.");

        String method = clientMessage.getMethod();

        switch (method) {
            case "helloWorld": {
                String output = helloWorld();
                ServerMessage resp = null;
                if(clientMessage.getParams() != null) {
                    resp = new ServerMessage(clientMessage, new Value(output));
                } else {
                    resp = new ServerMessage(clientMessage, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
                }
                Library.getServer().reply(resp, clientMessage, false);
                break;
            }
            case "sum": {
                Value params = clientMessage.getParams();
                ServerMessage resp = null;
                if(params != null && params.isArray() && params.getArraySize() == 2) {
                    try {
                        int n1 = ((Number) params.getArrayElement(0).getPrimitive()).intValue();
                        int n2 = ((Number) params.getArrayElement(1).getPrimitive()).intValue();
                        int output = sum(n1, n2);
                        resp = new ServerMessage(clientMessage, new Value(output));
                    } catch (ClassCastException e) {
                        resp = new ServerMessage(clientMessage, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
                    }
                } else {
                    resp = new ServerMessage(clientMessage, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
                }
                Library.getServer().reply(resp, clientMessage, false);
                break;
            }
            case "subtract": {
                Value params = clientMessage.getParams();
                ServerMessage resp = null;
                if(params != null && params.isArray() && params.getArraySize() == 2) {
                    try {
                        double n1 = ((Number) params.getArrayElement(0).getPrimitive()).doubleValue();
                        double n2 = ((Number) params.getArrayElement(1).getPrimitive()).doubleValue();
                        double output = subtract(n1, n2);
                        resp = new ServerMessage(clientMessage, new Value(output));
                    } catch (ClassCastException e) {
                        resp = new ServerMessage(clientMessage, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
                    }
                } else {
                    resp = new ServerMessage(clientMessage, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
                }
                Library.getServer().reply(resp, clientMessage, false);
                break;
            }
            case "randomInteger": {
                Value params = clientMessage.getParams();
                ServerMessage resp = null;
                if(params != null && params.isMap() && params.getMapKeySet().contains("min") && params.getMapKeySet().contains("max")) {
                    try {
                        int n1 = ((Number) params.getMapElement("min").getPrimitive()).intValue();
                        int n2 = ((Number) params.getMapElement("max").getPrimitive()).intValue();
                        int randomNum = random(n1, n2);
                        resp = new ServerMessage(clientMessage, new Value(randomNum));
                    } catch (ClassCastException e) {
                        resp = new ServerMessage(clientMessage, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
                    }
                } else {
                    resp = new ServerMessage(clientMessage, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
                }
                Library.getServer().reply(resp, clientMessage, false);
                break;
            }
            case "complexParams": {
                Value params = clientMessage.getParams();
                ServerMessage resp = null;
                //{"min": {"array":[Number, Boolean]}, "max": Boolean}
                if(params != null && params.mapHasKey("min") && params.mapHasKey("max")) {
                    try {
                        Value min = params.getMapElement("min");
                        Value max = params.getMapElement("max");
                        if(max.getPrimitive() instanceof Boolean && min.isMap() && min.mapHasKey("array")) {
                            Value array = min.getMapElement("array");
                            if(array.isArray() && array.getArraySize() == 2 && array.getArrayElement(0).getPrimitive() instanceof Number && array.getArrayElement(1).getPrimitive() instanceof  Boolean) {
                                resp = new ServerMessage(clientMessage, new Value("Correct parameters passed"));
                            } else {
                                resp = new ServerMessage(clientMessage, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
                            }
                        } else {
                            resp = new ServerMessage(clientMessage, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
                        }
                    } catch (ClassCastException e) {
                        resp = new ServerMessage(clientMessage, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
                    }
                } else {
                    resp = new ServerMessage(clientMessage, ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_PARAMS, null));
                }
                Library.getServer().reply(resp, clientMessage, false);
                break;
            }
        }
    }

    @Override
    public void onClientBatchMessageReceived(ClientMessage[] clientBatchMessage, Id id, String ip, int port) {
        callbackCalled = true;
        printlnWithRecursion("ClientMessage batch received");
    }

    @Override
    public void onServerMessageReceived(ServerMessage serverMessage, Id id, String ip, int port) {
        callbackCalled = true;
        printlnWithRecursion("ServerMessage received");
    }

    @Override
    public void onServerBatchMessageReceived(ServerMessage[] serverBatchMessage, Id id, String ip, int port) {
        callbackCalled = true;
        printlnWithRecursion("ServerMessage batch received");
    }

    private String helloWorld() {
        return "Hello world!";
    }

    private int sum(int x, int y) {
        return x+y;
    }

    private double subtract(double x, double y) {
        return x-y;
    }

    private int random(int x, int y) {
        return ThreadLocalRandom.current().nextInt(x, y + 1);
    }





    //Methods for input management
    private Pair<String, Integer> selectIPAndPort() {

        System.out.print("Insert broker IP. Leave blank to choose the default ip and port: " + IP + " - " + PORT + ".");
        String ip = scanner.nextLine();
        if(ip.equals("")) {
            return new Pair<>(IP, PORT);
        }
        int port = 0;
        while(true) {
            System.out.print("Insert broker PORT: ");
            try {
                port = Integer.valueOf(scanner.nextLine());
                break;
            } catch(NumberFormatException e) {
                printlnWithRecursion("Wrong number format");
            }
        }
        return new Pair<>(ip, port);
    }

    private Value getRegisterParams(String method, String ownerStr, String titleStr, String descriptionStr, String sectorStr, String[] keywordsArr, String input, String output) {
        Value identifier = new Value(method);
        Value owner = new Value(ownerStr);
        Value title = new Value(titleStr);
        Value description = new Value(descriptionStr);
        Value sector = new Value(sectorStr);

        Value keywords = Value.createAsArrayValue();
        for (String key : keywordsArr) {
            Value k = new Value(key);
            keywords.addArrayElement(k);
        }

        Value inputformat = new Value(input);
        Value outputformat = new Value(output);

        Value registerParams = new Value("identifier", identifier);
        registerParams.add("owner", owner);
        registerParams.add("title", title);
        registerParams.add("description", description);
        registerParams.add("sector", sector);
        registerParams.add("keywords", keywords);
        registerParams.add("inputformat", inputformat);
        registerParams.add("outputformat", outputformat);
        return registerParams;
    }

    private Value askForParams() {

        printlnWithRecursion("Does this method require any parameters?");
        String[] options = {"yes", "no"};
        int option = promptForOption(options, false);

        switch (option) {
            case 0:
                return requestValue();
            case 1:
                return null;
        }
        return null;
    }

    private Value requestValue() {

        printlnWithRecursion("Choose the structure type:");

        ArrayList<String> typesList = new ArrayList<>();
        typesList.add("Map");
        typesList.add("Array");
        typesList.add("Single");

        if (levelOfRecursion==0) {
            typesList.remove("Single");
        }

        //Ask for the parent structure type
        String[] types = typesList.toArray(new String[typesList.size()]);

        int type = promptForOption(types, true);

        if(type == 2) {
            levelOfRecursion++;
            printlnWithRecursion("Choose the single value type");
            String[] singleTypes = {"string", "number", "boolean", "null value"};
            int singleType = promptForOption(singleTypes, true);
            switch (singleType) {
                case 0:
                    printlnWithRecursion("Insert the string value");
                    decreaseRecursionLevel();
                    return new Value(scanner.nextLine());
                case 1:
                    printlnWithRecursion("Insert the number value");
                    String line = scanner.nextLine();
                    try{
                        Value ret = new Value(Integer.parseInt(line));
                        decreaseRecursionLevel();
                        return ret;
                    }catch (NumberFormatException e){
                        try{
                            Value ret = new Value(Double.parseDouble(line));
                            decreaseRecursionLevel();
                            return ret;
                        }catch (NumberFormatException ex){
                            printlnWithRecursion("Invalid option");
                        }
                    }
                    return null;
                case 2:
                    printlnWithRecursion("Insert the boolean value");
                    decreaseRecursionLevel();
                    return new Value(Boolean.parseBoolean(scanner.nextLine()));
                case 3:
                    decreaseRecursionLevel();
                    return new Value();
            }
        } else if(type == 0) {
            //Set the type
            Value parent = Value.createAsMapValue();
            printlnWithRecursion("Map value created. Entering the map.");
            levelOfRecursion++;
            //Fill in the nested structures
            while(true) {
                printlnWithRecursion("Choose an operation");
                String[] nestedActions = {"Add element", "Close this structure"};
                int choice = promptForOption(nestedActions, true);
                if (choice == 0) {
                    printlnWithRecursion("Insert the key");
                    String key = scanner.nextLine();
                    Value single = requestValue();
                    parent.add(key, single);
                } else {
                    decreaseRecursionLevel();
                    return parent;
                }
            }

        } else {
            Value parent = Value.createAsArrayValue();

            printlnWithRecursion("Array value created. Entering the array.");
            levelOfRecursion++;
            //Fill in the nested structures
            while(true) {
                printlnWithRecursion("Choose an operation");
                String[] nestedActions = {"Add element", "Close this structure"};
                int choice = promptForOption(nestedActions, true);
                if (choice == 0) {
                    Value single = requestValue();
                    parent.addArrayElement(single);
                } else {
                    printlnWithRecursion("Going back to the previous structure.");
                    decreaseRecursionLevel();
                    return parent;
                }
            }
        }
        return null;
    }

    private int promptForOption(String[] options, boolean inline) {
        int option = 0;
        do {
            if(inline && options.length > 0) {
                printWithRecursion(1 + ": " + options[0]);
                System.out.print("; ");
                for (int i = 1; i < options.length; i++) {
                    System.out.print((i + 1) + ": " + options[i]);
                    System.out.print("; ");
                }
                printlnWithRecursion("");
            } else {
                for (int i = 0; i < options.length; i++) {
                    printlnWithRecursion((i + 1) + ": " + options[i]);
                }
            }
            try{
                option = Integer.parseInt(scanner.nextLine());
            }catch (NumberFormatException e){
                printlnWithRecursion("Invalid option");
            }
        } while (option < 1 || option > options.length+1);
        return option-1;
    }

    private ClientMessage.ClientMessageBuilder requestId(ClientMessage.ClientMessageBuilder builder) {
        printlnWithRecursion("Please choose the desired id type.");
        String[] singleTypes = {"choose automatically", "string", "number", "null id"};
        int singleType = promptForOption(singleTypes, true);
        switch (singleType) {
            case 0:
                autoIndex++;
                return builder.id(autoIndex);
            case 1:
                printlnWithRecursion("Insert the string value");
                return builder.id(scanner.nextLine());
            case 2:
                printlnWithRecursion("Insert the number value");
                String line = scanner.nextLine();
                try{
                    int i =Integer.parseInt(line);
                    return builder.id(i);
                }catch (NumberFormatException e){
                    try{
                        double d = Double.parseDouble(line);
                        return builder.id(d);
                    }catch (NumberFormatException ex){
                        printlnWithRecursion("Invalid option. Sending a notification...");
                    }
                }
                return builder;
            case 3:
                return builder.nullId();
                default:
                    return builder;
        }
    }





    //Methods for printing with recursion on the console
    private void printWithRecursion(String s) {
        for(int i = 0; i < levelOfRecursion; i++) {
            System.out.print("       ");
        }

        System.out.print(s);
    }

    private void printlnWithRecursion(String s) {
        for(int i = 0; i < levelOfRecursion; i++) {
            System.out.print("       ");
        }

        System.out.println(s);
    }

    private void decreaseRecursionLevel() {
        if(levelOfRecursion >0) {
            levelOfRecursion--;
        }
    }
}
