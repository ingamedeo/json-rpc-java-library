package group_2.library.facades;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import group_2.library.Pair;
import group_2.library.Utils;
import group_2.library.callbacks.ClientMessageCallback;
import group_2.library.callbacks.ServerMessageCallback;
import group_2.library.channels.AbstractChannelFactory;
import group_2.library.channels.ListenChannel;
import group_2.library.channels.SendChannel;
import group_2.library.entities.ClientMessage;
import group_2.library.entities.Id;
import group_2.library.entities.ServerMessage;
import group_2.library.entities.Value;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/*
 *
 * We are testing that the JRPCServer/JRPCClient actually receive and send messages over the network channel through the Library.
 * In particular, extended tests are conducted on the send/receive functionality of each of the two client-side high-level abstractions.
 *
 * This is done using MockChannels (further proof that the Library is agnostic with respect to the network channel)
 *
 * */

public class  LibraryTest {

    private static final int TIMEOUTMS = 100;

    private static final Gson gson = Utils.prepareGson();

    private CountDownLatch lock1 = new CountDownLatch(1);
    private CountDownLatch lock2 = new CountDownLatch(1);

    //channel allowed to receive fake message
    private static boolean allowedToReceiveServerMessage = false;
    private static boolean allowedToReceiveClientMessage = false;
    private static boolean allowedToReceiveBatchServerMessage = false;
    private static boolean allowedToReceiveBatchClientMessage = false;

    //client message received by channel
    private static boolean clientMessageReceivedByChannel = false;
    private static boolean clientBatchMessageReceivedByChannel = false;

    //server message received by channel
    private static boolean serverMessageReceivedByChannel = false;
    private static boolean serverBatchMessageReceivedByChannel = false;

    private boolean clientMessageReceivedByJRPCServer = false;
    private boolean clientBatchMessageReceivedByJRPCServer = false;

    private boolean serverMessageReceivedByJRPCClient = false;
    private boolean serverBatchMessageReceivedByJRPCClient = false;

    @Before
    public void setUp() throws Exception {
        Library.init(new MockChannelFactory());
    }

    @After
    public void tearDown() throws Exception {
        Library.stopLibrary();
    }

    @Test
    public void getServer() throws Exception {

        JRPCServer jrpcServer = Library.getServer();
        jrpcServer.registerServer("0", 0, new ClientMessageCallback() {
            @Override
            public void onClientMessageReceived(ClientMessage clientMessage, Id id, String ip, int port) {
                clientMessageReceivedByJRPCServer = true;
                ServerMessage serverMessage = new ServerMessage("re", new Value("Ok"));
                jrpcServer.reply(serverMessage, clientMessage, false);
            }

            @Override
            public void onClientBatchMessageReceived(ClientMessage[] clientBatchMessage, Id id, String ip, int port) {
                clientBatchMessageReceivedByJRPCServer = true;
                ServerMessage serverMessage1 = new ServerMessage(1, new Value("Ok"));
                ServerMessage serverMessage2 = new ServerMessage(2, new Value("Ok"));
                jrpcServer.reply(serverMessage1, clientBatchMessage[0],true);
                jrpcServer.reply(serverMessage2, clientBatchMessage[1],true);
            }
        });

        allowedToReceiveClientMessage = true; //Start sending on ListenChannel
        allowedToReceiveBatchClientMessage = true;
        lock1.await(TIMEOUTMS, TimeUnit.MILLISECONDS);

        assertTrue(serverMessageReceivedByChannel);
        assertTrue(serverBatchMessageReceivedByChannel);

        assertTrue(clientMessageReceivedByJRPCServer);
        assertTrue(clientBatchMessageReceivedByJRPCServer);
    }

    @Test
    public void getClient() throws Exception {
        JRPCClient jrpcClient = Library.getClient();
        ClientMessage clientMessage = new ClientMessage.ClientMessageBuilder("method").id(1).build();
        jrpcClient.sendClientMessage("0", 0, clientMessage);

        ClientMessage clientMessage1 = new ClientMessage.ClientMessageBuilder("method1").id(1).build();
        ClientMessage clientMessage2 = new ClientMessage.ClientMessageBuilder("method2").id(2).build();
        ClientMessage[] clientMessages = {clientMessage1, clientMessage2};

        jrpcClient.sendBatchClientMessage("0", 0, clientMessages);

        //sendClientMessage has successfully delivered our clientMessage to the channel implementation.
        assertTrue(clientMessageReceivedByChannel);
        assertTrue(clientBatchMessageReceivedByChannel);

        jrpcClient.registerForServerMessages(new ServerMessageCallback() {
            @Override
            public void onServerMessageReceived(ServerMessage serverMessage, Id id, String ip, int port) {
                serverMessageReceivedByJRPCClient = true;
            }

            @Override
            public void onServerBatchMessageReceived(ServerMessage[] serverBatchMessage, Id id, String ip, int port) {
                serverBatchMessageReceivedByJRPCClient = true;
            }
        });

        allowedToReceiveServerMessage = true;
        allowedToReceiveBatchServerMessage = true;
        lock2.await(TIMEOUTMS, TimeUnit.MILLISECONDS);

        assertTrue(serverMessageReceivedByJRPCClient);
        assertTrue(serverBatchMessageReceivedByJRPCClient);

    }

    private static class MockListenChannel implements ListenChannel{

        @Override
        public void bind(String ip, int port) {

        }

        @Override
        public void close() {

        }

        @Override
        public Pair<Id, String> receive() {

            while (!allowedToReceiveServerMessage && !allowedToReceiveClientMessage && !allowedToReceiveBatchServerMessage && !allowedToReceiveBatchClientMessage) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    System.out.println("MockListenChannel stopped");
                }
            }

            if (allowedToReceiveServerMessage) {
                allowedToReceiveServerMessage = false;
                return  new Pair<>(new Id("test".getBytes()), "{\"jsonrpc\": \"2.0\", \"result\": 19, \"id\": 1}");
            }

            if (allowedToReceiveClientMessage){
                allowedToReceiveClientMessage = false;
                return  new Pair<>(new Id("test".getBytes()), "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [42, 23], \"id\": 1}");
            }

            if (allowedToReceiveBatchClientMessage) {
                allowedToReceiveBatchClientMessage = false;
                return  new Pair<>(new Id("test".getBytes()), "[{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [42, 23], \"id\": 1}, {\"jsonrpc\": \"2.0\", \"method\": \"add\", \"id\": 2}]");
            }

            if (allowedToReceiveBatchServerMessage) {
                allowedToReceiveBatchServerMessage = false;
                return  new Pair<>(new Id("test".getBytes()), "[{\"jsonrpc\": \"2.0\", \"result\": 19, \"id\": 1}, {\"jsonrpc\": \"2.0\", \"result\": 10, \"id\": 2}]");
            }

            return null;
        }

        @Override
        public boolean send(Id id, String message) {

            try {
                ClientMessage clientMessage = gson.fromJson(message, ClientMessage.class);
                ServerMessage serverMessage = gson.fromJson(message, ServerMessage.class);

                if (clientMessage!=null) {
                    clientMessageReceivedByChannel = true;
                }

                if (serverMessage!=null) {
                    serverMessageReceivedByChannel = true;
                }

            } catch (JsonSyntaxException e) {
                ClientMessage[] clientMessages = gson.fromJson(message, ClientMessage[].class);
                ServerMessage[] serverMessages = gson.fromJson(message, ServerMessage[].class);

                boolean isClient = !Utils.isAllNullArray(clientMessages);
                boolean isServer = !Utils.isAllNullArray(serverMessages);

                if (isClient) {
                    clientBatchMessageReceivedByChannel = true;
                }

                if (isServer) {
                    serverBatchMessageReceivedByChannel = true;
                }

            }

            return true;
        }
    }

    private static class MockSendChannel implements SendChannel{

        @Override
        public void connect(String ip, int port) {

        }

        @Override
        public boolean send(String message) {

            try {
                ClientMessage clientMessage = gson.fromJson(message, ClientMessage.class);
                ServerMessage serverMessage = gson.fromJson(message, ServerMessage.class);

                if (clientMessage!=null) {
                    clientMessageReceivedByChannel = true;
                }

                if (serverMessage!=null) {
                    serverMessageReceivedByChannel = true;
                }

            } catch (JsonSyntaxException e) {
                ClientMessage[] clientMessages = gson.fromJson(message, ClientMessage[].class);
                ServerMessage[] serverMessages = gson.fromJson(message, ServerMessage[].class);

                boolean isClient = !Utils.isAllNullArray(clientMessages);
                boolean isServer = !Utils.isAllNullArray(serverMessages);

                if (isClient) {
                    clientBatchMessageReceivedByChannel = true;
                }

                if (isServer) {
                    serverBatchMessageReceivedByChannel = true;
                }

            }

            return true;
        }

        @Override
        public String receive() {

            while (!allowedToReceiveServerMessage && !allowedToReceiveClientMessage && !allowedToReceiveBatchServerMessage && !allowedToReceiveBatchClientMessage) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    System.out.println("MockListenChannel stopped");
                }
            }

            if (allowedToReceiveServerMessage) {
                allowedToReceiveServerMessage = false;
                return  "{\"jsonrpc\": \"2.0\", \"result\": 19, \"id\": 1}";
            }

            if (allowedToReceiveClientMessage){
                allowedToReceiveClientMessage = false;
                return  "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [42, 23], \"id\": 1}";
            }

            if (allowedToReceiveBatchClientMessage) {
                allowedToReceiveBatchClientMessage = false;
                return  "[{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [42, 23], \"id\": 1}, {\"jsonrpc\": \"2.0\", \"method\": \"add\", \"id\": 2}]";
            }

            if (allowedToReceiveBatchServerMessage) {
                allowedToReceiveBatchServerMessage = false;
                return  "[{\"jsonrpc\": \"2.0\", \"result\": 19, \"id\": 1}, {\"jsonrpc\": \"2.0\", \"result\": 10, \"id\": 2}]";
            }

            return null;
        }

        @Override
        public void close() {

        }
    }

    private static class MockChannelFactory extends AbstractChannelFactory{

        @Override
        public ListenChannel createListenChannel() {
            return new MockListenChannel();
        }

        @Override
        public SendChannel createSendChannel() {
            return new MockSendChannel();
        }
    }
}

