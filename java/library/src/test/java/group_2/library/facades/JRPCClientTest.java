package group_2.library.facades;

import group_2.library.Pair;
import group_2.library.callbacks.ClientMessageCallback;
import group_2.library.channels.AbstractChannelFactory;
import group_2.library.channels.ListenChannel;
import group_2.library.channels.SendChannel;
import group_2.library.entities.ClientMessage;
import group_2.library.entities.Id;
import group_2.library.entities.Value;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class JRPCClientTest {

    /*
     * This class tests the send API of the library. The correctness of the message sent on the network
     * is checked here. Another test checks the correctness of the received message.
     */

    private String sentSendChannelMessage = null;
    private String toBeReceivedSendChannelMessage = null;

    @Before
    public void setUp() throws Exception {
        //Start by creating a channel factory
        AbstractChannelFactory mockFactory = new MockFactory();

        //Initialise JSON-RPC library providing way to use underlying network impl.
        Library.init(mockFactory);

        //Register new JSON-RPC server
        JRPCServer jrpcServer = Library.getServer();
        jrpcServer.registerServer("0", 0, new MockCallback());
    }

    @After
    public void tearDown() throws Exception {
        Library.stopLibrary();
    }

    @Test
    public void sendClientMessage() throws Exception {

        ClientMessage[] validClientMessageObjects = getMessages();

        //Expected results
        String[] validStringSet = new String[] {

                //No params
                "{\"jsonrpc\":\"2.0\",\"id\":1.0,\"method\":\"test\"}",

                //with positional parameters
                "{\"jsonrpc\":\"2.0\",\"id\":2,\"method\":\"subtract\",\"params\":[23,42]}",

                //with named parameters
                "{\"jsonrpc\":\"2.0\",\"id\":3,\"method\":\"subtract\",\"params\":{\"subtrahend\":23,\"minuend\":42}}",

                //notification and mixed type array
                "{\"jsonrpc\":\"2.0\",\"method\":\"subtract\",\"params\":[23,42,\"test\"]}",

                //Test with not recommmended fractional id and fractional parameters
                "{\"jsonrpc\":\"2.0\",\"id\":1.5,\"method\":\"subtract\",\"params\":[23.0]}",

                //null id
                "{\"jsonrpc\":\"2.0\",\"id\":null,\"method\":\"subtract\",\"params\":[23.0]}",
        };

        //Test with invalid message with null method. Must not be sent
        ClientMessage noMethod = new ClientMessage.ClientMessageBuilder(null).id(3).build();
        try {
            Library.getClient().sendClientMessage("0", 0, noMethod, null);
            fail("Null method test failed");
        } catch (IllegalArgumentException e) {
            System.out.println("Null method test passed");
        }

        //Tests with valid messages
        for(int i = 0; i < validStringSet.length; i++) {
            Library.getClient().sendClientMessage("0", 0, validClientMessageObjects[i], null);
            assertEquals(validStringSet[i], sniffMessage());
            System.out.println("Test with message " + i + " passed");
        }
    }

    @Test
    public void sendBatchClientMessage() throws Exception {
        //Test a sample batch message
        ClientMessage[] validClientMessageObjects = getMessages();
        Library.getClient().sendBatchClientMessage("0", 0, validClientMessageObjects, null);
        assertEquals("[{\"jsonrpc\":\"2.0\",\"id\":1.0,\"method\":\"test\"},{\"jsonrpc\":\"2.0\",\"id\":2,\"method\":\"subtract\",\"params\":[23,42]},{\"jsonrpc\":\"2.0\",\"id\":3,\"method\":\"subtract\",\"params\":{\"subtrahend\":23,\"minuend\":42}},{\"jsonrpc\":\"2.0\",\"method\":\"subtract\",\"params\":[23,42,\"test\"]},{\"jsonrpc\":\"2.0\",\"id\":1.5,\"method\":\"subtract\",\"params\":[23.0]},{\"jsonrpc\":\"2.0\",\"id\":null,\"method\":\"subtract\",\"params\":[23.0]}]",
                sniffMessage());
    }

    private String sniffMessage() {
        int count = 0;
        while(true) {
            if(sentSendChannelMessage != null) {
                String s = sentSendChannelMessage;
                sentSendChannelMessage = null;
                return s;
            } else {
                count++;
                if(count > 10) {
                    fail("Waited for too long");
                }
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private ClientMessage[] getMessages() {
        //Parameters building section
        Value p1 = new Value(23);
        Value p2 = new Value(42);
        Value p3 = new Value("test");
        Value p4 = new Value(23.0);

        Value params1 = new Value(p1); params1.addArrayElement(p2);
        Value params2 = new Value("subtrahend", p1); params2.add("minuend", p2);
        Value params3 = new Value(p1); params3.addArrayElement(p2); params3.addArrayElement(p3);
        Value params4 = Value.createAsArrayValue(); params4.addArrayElement(p4);

        //Messages ready to test
        ClientMessage[] ret = {
                new ClientMessage.ClientMessageBuilder("test").id(1.0).build(),
                new ClientMessage.ClientMessageBuilder("subtract").id(2).params(params1).build(),
                new ClientMessage.ClientMessageBuilder("subtract").id(3).params(params2).build(),
                new ClientMessage.ClientMessageBuilder("subtract").params(params3).build(),
                new ClientMessage.ClientMessageBuilder("subtract").id(1.5).params(params4).build(),
                new ClientMessage.ClientMessageBuilder("subtract").nullId().params(params4).build()
        };
        return ret;
    }


    private class MockSendChannel implements SendChannel {

        @Override
        public void connect(String ip, int port) {
        }

        @Override
        public boolean send(String message) {
            sentSendChannelMessage = message;
            return true;
        }

        @Override
        public String receive() {
            while(true) {
                if(toBeReceivedSendChannelMessage != null) {
                    String s = toBeReceivedSendChannelMessage;
                    toBeReceivedSendChannelMessage = null;
                    return s;
                } else {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        System.out.println("Send channel stopped");
                    }
                }
            }
        }

        @Override
        public void close() {

        }
    }


    public class MockListenChannel implements ListenChannel {

        @Override
        public void bind(String ip, int port) {
        }

        @Override
        public void close() {
        }

        @Override
        public Pair<Id, String> receive() {
            while(true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    //Program terminated
                }
            }
        }

        @Override
        public boolean send(Id id, String message) {
            return true;
        }
    }



    public class MockFactory extends AbstractChannelFactory {

        @Override
        public ListenChannel createListenChannel() {
            return new MockListenChannel();
        }

        @Override
        public SendChannel createSendChannel() {
            return new MockSendChannel();
        }
    }

    //Not implemented, but it is needed to start the library
    public class MockCallback implements ClientMessageCallback {

        @Override
        public void onClientMessageReceived(ClientMessage clientMessage, Id id, String ip, int port) {
        }

        @Override
        public void onClientBatchMessageReceived(ClientMessage[] clientBatchMessage, Id id, String ip, int port) {
        }
    }

}