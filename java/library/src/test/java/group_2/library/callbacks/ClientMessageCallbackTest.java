package group_2.library.callbacks;

import group_2.library.Pair;
import group_2.library.channels.AbstractChannelFactory;
import group_2.library.channels.ListenChannel;
import group_2.library.channels.SendChannel;
import group_2.library.entities.ClientMessage;
import group_2.library.entities.Id;
import group_2.library.facades.JRPCServer;
import group_2.library.facades.Library;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;

import static org.junit.Assert.*;

public class ClientMessageCallbackTest {

     /*
     * This class tests the receive API of the library, represented by these callbacks. Here we check that the valid messages
     * are translated into the correct objects, and that the invalid messages are rejected.
     */

    private String toBeReceivedListenChannelMessage = null;
    private String sentByListenChannelMessage = null;
    private ClientMessage receivedListenMessage = null;
    private ClientMessage[] receivedBatchMessage = null;

    @Before
    public void setUp() throws Exception {
        //Start by creating a channel factory
        AbstractChannelFactory mockFactory = new MockFactory();

        //Initialise JSON-RPC library providing way to use underlying network impl.
        Library.init(mockFactory);

        //Register new JSON-RPC server
        JRPCServer jrpcServer = Library.getServer();
        jrpcServer.registerServer("0", 0, new MockCallback());
    }

    @After
    public void tearDown() throws Exception {
        Library.stopLibrary();
    }


    @Test
    public void onClientMessageReceived() throws Exception {

        String[] invalidJson = new String[] {
                "{\"jsonrpc\": \"2.0\", \"method\": \"foobar, \"params\": \"bar\", \"baz]",
                "randtext"
        };

        String[] invalidClientMessageSet = new String[] {

                //invalid json version
                "{\"jsonrpc\": \"3.0\", \"method\": \"subtract\", \"params\": [23, 42], \"id\": 1}",

                //invalid params (primitive)
                "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": 10, \"id\": 1}",

                //Valid json, but invalid ClientMessage
                "{\"randomtext\": 1, \"randomtext1\": 1, \"randomtext2\": 1, \"randomtext3\": 1}",
                "null", "{}", "\"hello\"", "[]", "3",

                //Additional parameters
                "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [23, 42], \"id\": 1, \"test\":3}",
                "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"id\": 1, \"test\":3}",
                "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [23, 42], \"test\":3}",
                "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"test\":3}",

                //Missing mandatory parameters
                "{\"method\": \"subtract\", \"params\": [23, 42], \"id\": 1}",
                "{\"jsonrpc\": \"2.0\", \"params\": [23, 42], \"id\": 1}",

                //Wrong method type
                "{\"jsonrpc\": \"2.0\", \"method\": 3, \"params\": [23, 42], \"id\": 1}",


        };

        String[] validStringSet = new String[] {

                //No params
                "{\"jsonrpc\":\"2.0\",\"id\":1.0,\"method\":\"test\"}",

                //with positional parameters
                "{\"jsonrpc\":\"2.0\",\"id\":2,\"method\":\"subtract\",\"params\":[23,42]}",

                //with named parameters
                "{\"jsonrpc\":\"2.0\",\"id\":3,\"method\":\"subtract\",\"params\":{\"subtrahend\":23,\"minuend\":42}}",

                //notification and mixed type array
                "{\"jsonrpc\":\"2.0\",\"method\":\"subtract\",\"params\":[23,42,\"test\"]}",

                //Test with not recommmended fractional id and fractional parameters
                "{\"jsonrpc\":\"2.0\",\"id\":1.5,\"method\":\"subtract\",\"params\":[23.0]}",

                //null id
                "{\"jsonrpc\":\"2.0\",\"id\":null,\"method\":\"subtract\",\"params\":[23.0]}",
        };



        //First test: invalid json -> the library should not accept the message and respond with a parse error
        for(int i = 0; i < invalidJson.length; i++) {
            toBeReceivedListenChannelMessage = invalidJson[i];
            assertEquals("{\"jsonrpc\":\"2.0\",\"id\":null,\"error\":{\"code\":-32700,\"message\":\"Parse error\"}}", waitForResponse());
            System.out.println("Passed invalid json test n " + i);
        }


        //Second test: invalid request -> the library should not accept the message and respond with an invalid request error
        for(int i = 0; i < invalidClientMessageSet.length; i++) {
            toBeReceivedListenChannelMessage = invalidClientMessageSet[i];
            assertEquals("{\"jsonrpc\":\"2.0\",\"id\":null,\"error\":{\"code\":-32600,\"message\":\"Invalid Request\"}}", waitForResponse());
            System.out.println("Passed invalid request test n " + i);
        }


        ClientMessage msg = null;
        toBeReceivedListenChannelMessage = validStringSet[0];
        msg=waitForCallback();
        assertEquals("test", msg.getMethod());
        assertEquals(1, ((Number)msg.getId()).intValue());
        assertEquals(false, msg.isNotification());

        toBeReceivedListenChannelMessage = validStringSet[1];
        msg=waitForCallback();
        System.out.println(msg);
        assertEquals("subtract", msg.getMethod());
        assertEquals(2, ((Number)msg.getId()).intValue());
        assertEquals(23, ((Number)msg.getParams().getArrayElement(0).getPrimitive()).intValue());
        assertEquals(42, ((Number)msg.getParams().getArrayElement(1).getPrimitive()).intValue());
        assertEquals(false, msg.isNotification());

        toBeReceivedListenChannelMessage = validStringSet[2];
        msg=waitForCallback();
        assertEquals("subtract", msg.getMethod());
        assertEquals(3, ((Number)msg.getId()).intValue());
        assertEquals(23, ((Number)msg.getParams().getMapElement("subtrahend").getPrimitive()).intValue());
        assertEquals(42, ((Number)msg.getParams().getMapElement("minuend").getPrimitive()).intValue());
        assertEquals(false, msg.isNotification());

        toBeReceivedListenChannelMessage = validStringSet[3];
        msg=waitForCallback();
        assertEquals("subtract", msg.getMethod());
        assertEquals(23, ((Number)msg.getParams().getArrayElement(0).getPrimitive()).intValue());
        assertEquals(42, ((Number)msg.getParams().getArrayElement(1).getPrimitive()).intValue());
        assertEquals(true, msg.isNotification());

        toBeReceivedListenChannelMessage = validStringSet[4];
        msg=waitForCallback();
        assertEquals("subtract", msg.getMethod());
        assertEquals(1.5, ((Number)msg.getId()).doubleValue(), 0.001);
        assertEquals(23.0, ((Number)msg.getParams().getArrayElement(0).getPrimitive()).intValue(), 0.001);
        assertEquals(false, msg.isNotification());

        toBeReceivedListenChannelMessage = validStringSet[5];
        msg=waitForCallback();
        assertEquals("subtract", msg.getMethod());
        assertNull(msg.getId());
        assertEquals(23.0, ((Number)msg.getParams().getArrayElement(0).getPrimitive()).intValue(), 0.001);
        assertEquals(false, msg.isNotification());

    }

    @Test
    public void onClientBatchMessageReceived() throws Exception {
        //Test a sample batch request
        toBeReceivedListenChannelMessage = "[{\"jsonrpc\":\"2.0\",\"id\":1.0,\"method\":\"test\"},{\"jsonrpc\":\"2.0\",\"id\":2,\"method\":\"subtract\",\"params\":[23,42]}]";

        ClientMessage[] msgs = waitForBatchCallback();
        ClientMessage msg = msgs[0];
        assertEquals("test", msg.getMethod());
        assertEquals(1, ((Number)msg.getId()).intValue());
        assertEquals(false, msg.isNotification());

        msg=msgs[1];
        assertEquals("subtract", msg.getMethod());
        assertEquals(2, ((Number)msg.getId()).intValue());
        assertEquals(23, ((Number)msg.getParams().getArrayElement(0).getPrimitive()).intValue());
        assertEquals(42, ((Number)msg.getParams().getArrayElement(1).getPrimitive()).intValue());
        assertEquals(false, msg.isNotification());
    }

    private ClientMessage waitForCallback() {
        int count = 0;
        while(true) {
            if(receivedListenMessage != null) {
                ClientMessage cm = receivedListenMessage;
                receivedListenMessage = null;
                return cm;
            } else {
                count++;
                if(count > 10) {
                    fail("Waited for too long");
                }
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private ClientMessage[] waitForBatchCallback() {
        int count = 0;
        while(true) {
            if(receivedBatchMessage != null) {
                ClientMessage[] cma = receivedBatchMessage;
                receivedBatchMessage = null;
                return cma;
            } else {
                count++;
                if(count > 10) {
                    fail("Waited for too long");
                }
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String waitForResponse() {
        int count = 0;
        while(true) {
            if(sentByListenChannelMessage != null) {
                String s = sentByListenChannelMessage;
                sentByListenChannelMessage = null;
                return s;
            } else {
                count++;
                if(count > 10) {
                    fail("Waited for too long");
                }
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private class MockSendChannel implements SendChannel {

        @Override
        public void connect(String ip, int port) {
        }

        @Override
        public boolean send(String message) {
            return true;
        }

        @Override
        public String receive() {
            while(true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    //Program terminated
                }
            }
        }

        @Override
        public void close() {

        }
    }

    public class MockListenChannel implements ListenChannel {

        @Override
        public void bind(String ip, int port) {
        }

        @Override
        public void close() {
        }

        @Override
        public Pair<Id, String> receive() {
            while(true) {
                if(toBeReceivedListenChannelMessage != null) {
                    String s = toBeReceivedListenChannelMessage;
                    toBeReceivedListenChannelMessage = null;
                    try {
                        return new Pair<>(new Id(String.valueOf(-1).getBytes("utf-8")), s);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        System.out.println("Listen channel stopped");
                    }
                }
            }

        }

        @Override
        public boolean send(Id id, String message) {
            sentByListenChannelMessage = message;
            return true;
        }
    }

    public class MockFactory extends AbstractChannelFactory {

        @Override
        public ListenChannel createListenChannel() {
            return new MockListenChannel();
        }

        @Override
        public SendChannel createSendChannel() {
            return new MockSendChannel();
        }
    }

    public class MockCallback implements ClientMessageCallback {

        @Override
        public void onClientMessageReceived(ClientMessage clientMessage, Id id, String ip, int port) {
            receivedListenMessage = clientMessage;
        }

        @Override
        public void onClientBatchMessageReceived(ClientMessage[] clientBatchMessage, Id id, String ip, int port) {
            receivedBatchMessage = clientBatchMessage;
        }
    }

}