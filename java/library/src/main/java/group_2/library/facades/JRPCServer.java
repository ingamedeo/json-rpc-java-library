package group_2.library.facades;

import group_2.library.callbacks.ClientMessageCallback;
import group_2.library.entities.ClientMessage;
import group_2.library.entities.ServerMessage;
import group_2.library.managers.ChannelManager;
import group_2.library.managers.ReceiveManager;
import group_2.library.managers.SendManager;

/*
 *
 * This class represents the role of a high-level JSONRPC Server, completely decoupled from the underlying network channels
 *
 * */

public class JRPCServer {

    public void registerForClientMessages(ClientMessageCallback callback) {
        ReceiveManager.getInstance().setClientMessageCallback(callback);
    }

    //registerListener
    public void registerServer(String ip, int port, ClientMessageCallback callback) {
        ChannelManager.getInstance().startListenChannel(ip, port);
        ReceiveManager.getInstance().setClientMessageCallback(callback);
    }

    public boolean reply(ServerMessage serverMessage, ClientMessage clientMessage, boolean batch) {
        return SendManager.getInstance().respond(serverMessage, clientMessage, batch);
    }
}
