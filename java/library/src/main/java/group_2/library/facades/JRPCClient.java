package group_2.library.facades;

import group_2.library.callbacks.ServerMessageCallback;
import group_2.library.entities.ClientMessage;
import group_2.library.entities.Id;
import group_2.library.managers.ReceiveManager;
import group_2.library.managers.SendManager;

/*
*
* This class represents the role of a high-level JSONRPC Client, completely decoupled from the underlying network channels
*
* */



public class JRPCClient {


    public void registerForServerMessages(ServerMessageCallback callback) {
        ReceiveManager.getInstance().setServerMessageCallback(callback);
    }

    public boolean sendClientMessage(String serverIp, int serverPort, ClientMessage clientMessage, Id id) {
        return SendManager.getInstance().sendClientMessage(clientMessage, serverIp, serverPort, id);
    }

    public boolean sendClientMessage(String serverIp, int serverPort, ClientMessage clientMessage) {
        return sendClientMessage( serverIp, serverPort, clientMessage,null);
    }

    public boolean sendBatchClientMessage(String serverIp, int serverPort, ClientMessage[] clientMessages) {
        return sendBatchClientMessage(serverIp, serverPort, clientMessages, null);
    }

    public boolean sendBatchClientMessage(String serverIp, int serverPort, ClientMessage[] clientMessages, Id id) {
        return SendManager.getInstance().sendBatchClientMessage(clientMessages, serverIp, serverPort, id);
    }
}
