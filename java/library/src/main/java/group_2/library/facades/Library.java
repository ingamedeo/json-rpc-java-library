package group_2.library.facades;

import group_2.library.channels.AbstractChannelFactory;
import group_2.library.managers.ChannelManager;

public class Library {

    //Used to initialize the Library, a factory to build network channels much be provided
    public static void init(AbstractChannelFactory channelFactory) {
        ChannelManager.getInstance().setChannelFactory(channelFactory);
    }

    public static JRPCServer getServer() {
        return new JRPCServer();
    }

    public static JRPCClient getClient() {
        return new JRPCClient();
    }

    //Used when the user no longer wants to use the library: kills channel threads, asks the channels to close network sockets, etc..
    public static void stopLibrary() {
        ChannelManager.getInstance().stopAllChannels();
    }
}
