package group_2.library.gsoncustom;

import com.google.gson.*;
import group_2.library.entities.Value;

import java.lang.reflect.Type;
import java.util.Set;

/*
*
* Adapter to build Value objects, from Value -> JSON and vice versa
* As there are multiple layers of objects (and the depth is not known beforehand) a recursive structure is necessary here.
*
* */

public class ValueAdapter implements JsonSerializer<Value>, JsonDeserializer<Value> {

    private Value buildValueObject(JsonElement jsonElement) {

        Value value = null;

        if (jsonElement.isJsonNull()) {
            value = new Value();
            return value;
        }

        if (jsonElement.isJsonPrimitive()) {

            JsonPrimitive jsonPrimitive = jsonElement.getAsJsonPrimitive();

            if (jsonPrimitive.isString()) {
                value = new Value(jsonPrimitive.getAsString());
            } else if (jsonPrimitive.isNumber()) {
                value = new Value(jsonPrimitive.getAsNumber());
            } else if (jsonPrimitive.isBoolean()) {
                value = new Value(jsonPrimitive.getAsBoolean());
            }

            return value;
        }

        if (jsonElement.isJsonObject()) {

            JsonObject jsonObject = jsonElement.getAsJsonObject();
            Set<String> keys = jsonObject.keySet();
            value = Value.createAsMapValue();
            for (String key : keys) {
                JsonElement element = jsonObject.get(key);
                Value valueInner = buildValueObject(element);
                value.add(key, valueInner);
            }

        }  else if (jsonElement.isJsonArray()) {

            JsonArray jsonArray = jsonElement.getAsJsonArray();
            value = Value.createAsArrayValue();
            for (JsonElement element : jsonArray) {
                Value valueInner = buildValueObject(element);
                 value.addArrayElement(valueInner);
            }
        }

        return value;
    }

    @Override
    public Value deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return buildValueObject(jsonElement);
    }

    private JsonElement getJsonFromValue(Value value, JsonSerializationContext jsonSerializationContext) {

        if (value.isNull()) {
            return JsonNull.INSTANCE;
        }

        if (value.isPrimitive()) {

            if (value.getPrimitive() instanceof String) {
                String s = String.valueOf(value.getPrimitive());
                return jsonSerializationContext.serialize(s, String.class);
            } else if (value.getPrimitive() instanceof Number) {
                Number n = (Number)value.getPrimitive();
                return jsonSerializationContext.serialize(n, Number.class);
            } else if (value.getPrimitive() instanceof Boolean) {
                Boolean n = (Boolean) value.getPrimitive();
                return jsonSerializationContext.serialize(n, Boolean.class);
            }

        }

        if (value.isMap()) {

            Set<String> keySet = value.getMapKeySet();

            JsonObject jsonObject = new JsonObject();

            for (String key : keySet) {
                Value valueFromKey = value.getMapElement(key);
                JsonElement valueJson = getJsonFromValue(valueFromKey, jsonSerializationContext);
                jsonObject.add(key, valueJson);
            }

            return jsonObject;

        } else if (value.isArray()) {

            int size = value.getArraySize();

            JsonArray jsonArray = new JsonArray();

            for (int i=0; i<size; i++) {
                Value valueFromIndex = value.getArrayElement(i);
                JsonElement valueJson = getJsonFromValue(valueFromIndex, jsonSerializationContext);
                jsonArray.add(valueJson);
            }

            return jsonArray;
        }

        return null;
    }

    @Override
    public JsonElement serialize(Value value, Type type, JsonSerializationContext jsonSerializationContext) {
        return getJsonFromValue(value, jsonSerializationContext);
    }
}
