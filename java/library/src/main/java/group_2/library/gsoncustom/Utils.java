package group_2.library.gsoncustom;

import com.google.gson.JsonNull;
import com.google.gson.JsonObject;

//GSON Utils class
public class Utils {

    private static final String idPropertyName = "id";

    protected static void addIdToJSON(Object id, JsonObject element) {
        if (id == null) {
            element.add(idPropertyName, JsonNull.INSTANCE);
        } else {
            if (id instanceof String) {
                element.addProperty(idPropertyName, (String) id);
            } else if (id instanceof Number) {
                element.addProperty(idPropertyName, (Number) id);
            }
        }
    }

}
