package group_2.library.gsoncustom;

import com.google.gson.*;
import group_2.library.entities.ErrorObject;
import group_2.library.entities.Value;

import java.lang.reflect.Type;

//GSON Adapter for ErrorObject, from JSON to ErrorObjects and vice versa
public class ErrorAdapter implements JsonSerializer<ErrorObject>, JsonDeserializer<ErrorObject> {

    private final static String CODE_PROPERTY_NAME = "code";
    private final static String MESSAGE_PROPERTY_NAME = "message";
    private final static String DATA_PROPERTY_NAME = "data";

    @Override
    public JsonElement serialize(ErrorObject error, Type type, JsonSerializationContext jsonSerializationContext) {

        JsonObject element = new JsonObject();

        element.addProperty(CODE_PROPERTY_NAME, error.getCode());

        if(error.getMessage() == null) {
            throw new IllegalArgumentException("Invalid JSONRPC: ErrorObject must have a message");
        }
        element.addProperty(MESSAGE_PROPERTY_NAME, error.getMessage());

        if (error.getData()!=null) {
            JsonElement dataElement = jsonSerializationContext.serialize(error.getData());
            element.add(DATA_PROPERTY_NAME, dataElement);
        }

        return element;
    }

    @Override
    public ErrorObject deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

        if (jsonElement.isJsonObject()) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();

            JsonElement code = jsonObject.get(CODE_PROPERTY_NAME);
            if(code == null || !code.isJsonPrimitive() || !code.getAsJsonPrimitive().isNumber()) {
                return null;
            }
            code = code.getAsJsonPrimitive();

            JsonElement message = jsonObject.get(MESSAGE_PROPERTY_NAME);
            if(message == null || !message.isJsonPrimitive() || !message.getAsJsonPrimitive().isString()) {
                return null;
            }
            message = message.getAsJsonPrimitive();

            if (jsonObject.has(DATA_PROPERTY_NAME)) {
                JsonElement dataJson = jsonObject.get(DATA_PROPERTY_NAME);
                Value data = jsonDeserializationContext.deserialize(dataJson, Value.class);
                return new ErrorObject(code.getAsInt(), message.getAsString(), data);
            } else {
                return new ErrorObject(code.getAsInt(), message.getAsString());
            }

        }
        return null;
    }
}
