package group_2.library.gsoncustom;

import com.google.gson.*;
import group_2.library.entities.ClientMessage;
import group_2.library.entities.Value;

import java.lang.reflect.Type;

//GSON Adapter for ClientMessages, from JSON to ClientMessages and vice versa
public class ClientMessageAdapter implements JsonSerializer<ClientMessage>, JsonDeserializer<ClientMessage> {

    private static final String ID_PROPERTY_NAME = "id";
    private static final String METHOD_PROPERTY_NAME = "method";
    private static final String JSONRPC_PROPERTY_NAME = "jsonrpc";
    private static final String PARAMS_PROPERTY_NAME = "params";

    private static final String JSONRPC_VERSION = "2.0";

    //Max number of keys in a JSON-RPC message
    private static final int JSONRPC_KEY_COUNT = 4;

    @Override
    public JsonElement serialize(ClientMessage clientMessage, Type type, JsonSerializationContext jsonSerializationContext) {

        JsonObject element = new JsonObject();

        element.addProperty(JSONRPC_PROPERTY_NAME, JSONRPC_VERSION);

        //Not a notification? There should be an ID field!
        if (!clientMessage.isNotification()) {
            Object id = clientMessage.getId();
            Utils.addIdToJSON(id, element);
        }

        if(clientMessage.getMethod() == null) {
            throw new IllegalArgumentException("Invalid JSON-RPC. Missing method.");
        }
        element.addProperty(METHOD_PROPERTY_NAME, clientMessage.getMethod());

        //Stop! Params can't be a primitive type.
        if (clientMessage.getParams()!=null && (clientMessage.getParams().isPrimitive() || clientMessage.getParams().isNull())) {
            throw new IllegalArgumentException("Invalid JSON-RPC. Params is of a primitive type or null.");
        }

        if(clientMessage.getParams() != null) {
            JsonElement valueElement = jsonSerializationContext.serialize(clientMessage.getParams());
            element.add(PARAMS_PROPERTY_NAME, valueElement);
        }

        return element;
    }

    @Override
    public ClientMessage deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

        ClientMessage clientMessage = null;

        //Deserializing a single ClientMessage
        if (jsonElement.isJsonObject()) {

            JsonObject jsonObject = jsonElement.getAsJsonObject();

            //If there are more than 4 keys, this has more keys than required.
            if (jsonObject.size() > JSONRPC_KEY_COUNT) {
                return null;
            }

            //Invalid JSON-RPC version!
            if (jsonObject.getAsJsonPrimitive(JSONRPC_PROPERTY_NAME) == null || !jsonObject.getAsJsonPrimitive(JSONRPC_PROPERTY_NAME).getAsString().equals(JSONRPC_VERSION)) {
                return null;
            }

            boolean notification = !jsonObject.has(ID_PROPERTY_NAME);
            JsonElement idElement = jsonObject.get(ID_PROPERTY_NAME);
            //JsonPrimitive id = jsonObject.getAsJsonPrimitive(ID_PROPERTY_NAME);

            JsonElement method = jsonObject.get(METHOD_PROPERTY_NAME);
            if(method == null || !method.isJsonPrimitive() || !method.getAsJsonPrimitive().isString()) {
                return null;
            }

            JsonElement valueElement = jsonObject.get(PARAMS_PROPERTY_NAME);
            Value value = jsonDeserializationContext.deserialize(valueElement, Value.class);
            if (value!=null && (value.isPrimitive() || value.isNull())) {
                return null;
            }

            if (value==null && !notification && jsonObject.size() > JSONRPC_KEY_COUNT-1) { //num keys = 3
                return null;
            } else if (value==null && notification && jsonObject.size() > JSONRPC_KEY_COUNT-2) { //num keys = 2
                return null;
            } else if (value!=null && notification && jsonObject.size() > JSONRPC_KEY_COUNT-1) { //num keys = 3
                return null;
            }

            Number idNumber = null;
            String idString = null;
            if (!notification) {
                if (!idElement.isJsonPrimitive() && !idElement.isJsonNull()) {
                    return null;
                }
                if (idElement.isJsonPrimitive() && ((JsonPrimitive)idElement).isNumber()) {
                    idNumber = idElement.getAsNumber();
                } else if (idElement.isJsonPrimitive() && ((JsonPrimitive)idElement).isString()) {
                    idString = idElement.getAsString();
                }
            }

            if (idNumber!=null) {
                clientMessage = new ClientMessage.ClientMessageBuilder(method.getAsString()).id(idNumber).params(value).build();
            } else if (idString!=null) {
                clientMessage = new ClientMessage.ClientMessageBuilder(method.getAsString()).id(idString).params(value).build();
            } else {

                ClientMessage.ClientMessageBuilder builder = new ClientMessage.ClientMessageBuilder(method.getAsString());

                if (!notification) {
                    builder.nullId(); //Not a notification, but id is null (Discouraged usage of ID field, section: 4, note [1])
                }

                clientMessage = builder.params(value).build();

            }

        } else {
            throw new JsonSyntaxException("Not a JsonObject");
        }

        return clientMessage;
    }
}
