package group_2.library.gsoncustom;

import com.google.gson.*;
import group_2.library.entities.ErrorObject;
import group_2.library.entities.ServerMessage;
import group_2.library.entities.Value;

import java.lang.reflect.Type;

//GSON Adapter for ServerMessages, from JSON to ServerMessages and vice versa
public class ServerMessageAdapter implements JsonSerializer<ServerMessage>, JsonDeserializer<ServerMessage> {

    private static final String ID_PROPERTY_NAME = "id";
    private static final String RESULT_PROPERTY_NAME = "result";
    private static final String JSONRPC_PROPERTY_NAME = "jsonrpc";
    private static final String ERROR_PROPERTY_NAME = "error";

    private static final String JSONRPC_VERSION = "2.0";

    //Max number of keys in a JSON-RPC message
    private static final int JSONRPC_KEY_COUNT = 3;

    @Override
    public JsonElement serialize(ServerMessage serverMessage, Type type, JsonSerializationContext jsonSerializationContext) {

        JsonObject element = new JsonObject();

        element.addProperty(JSONRPC_PROPERTY_NAME, JSONRPC_VERSION);

        Object id = serverMessage.getId();
        Utils.addIdToJSON(id, element);

        if (serverMessage.getError()!=null && serverMessage.getResult() == null) {
            //Ask gson to serialize ErrorObject
            JsonElement error = jsonSerializationContext.serialize(serverMessage.getError());
            element.add(ERROR_PROPERTY_NAME, error);
        } else if (serverMessage.getError()==null && serverMessage.getResult() != null) {
            JsonElement result = jsonSerializationContext.serialize(serverMessage.getResult());
            element.add(RESULT_PROPERTY_NAME, result);
        }

        return element;

    }

    private ServerMessage buildServerMessage(Number idNumber, String idString, Value result, ErrorObject error) {
        ServerMessage serverMessage = null;
        if (result!=null && idNumber!=null) {
            serverMessage = new ServerMessage(idNumber, result);
        } else if (result!=null && idString!=null) {
            serverMessage = new ServerMessage(idString, result);
        } else if (error!=null && idNumber!=null) {
            serverMessage = new ServerMessage(idNumber, error);
        } else if (error!=null && idString!=null) {
            serverMessage = new ServerMessage(idString, error);
        } else if (result!=null) {
            serverMessage = new ServerMessage(result);
        } else if (error!=null) {
            serverMessage = new ServerMessage(error);
        }

        return serverMessage;
    }

    @Override
    public ServerMessage deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        ServerMessage serverMessage = null;

        //Deserializing a single ClientMessage
        if (jsonElement.isJsonObject()) {

            JsonObject jsonObject = jsonElement.getAsJsonObject();

            //If there are more than 3 keys, this has more keys than required. OPT OUT.
            if (jsonObject.size() > JSONRPC_KEY_COUNT) {
                return null;
            }

            //Invalid JSON-RPC version!
            if (jsonObject.getAsJsonPrimitive(JSONRPC_PROPERTY_NAME) == null || !jsonObject.getAsJsonPrimitive(JSONRPC_PROPERTY_NAME).getAsString().equals(JSONRPC_VERSION)) {
                return null;
            }

            JsonElement idElement = jsonObject.get(ID_PROPERTY_NAME);
            if (idElement==null) {
                return null;
            }

            Number idNumber = null;
            String idString = null;

            if (!idElement.isJsonPrimitive() && !idElement.isJsonNull()) {
                return null;
            }
            if (idElement.isJsonPrimitive() && ((JsonPrimitive)idElement).isNumber()) {
                idNumber = idElement.getAsNumber();
            } else if (idElement.isJsonPrimitive() && ((JsonPrimitive)idElement).isString()) {
                idString = idElement.getAsString();
            }

            if (jsonObject.has(RESULT_PROPERTY_NAME)) {
                JsonElement result = jsonObject.get(RESULT_PROPERTY_NAME);
                Value value = jsonDeserializationContext.deserialize(result, Value.class);
                serverMessage = buildServerMessage(idNumber, idString, value, null);
            } else if (jsonObject.has(ERROR_PROPERTY_NAME)) {
                JsonElement error = jsonObject.get(ERROR_PROPERTY_NAME);
                ErrorObject e = jsonDeserializationContext.deserialize(error, ErrorObject.class);
                serverMessage = buildServerMessage(idNumber, idString, null, e);
            }
            //Else the method returns null
        } else {
            throw new JsonSyntaxException("Not a JsonObject");
        }

        return serverMessage;
    }
}
