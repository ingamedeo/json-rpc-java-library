package group_2.library.entities;

/*
* ID Wrapper
*  Wraps ID byte[], forces network channels to return an ID object.
*  Abstraction: The library isn't supposed to know what's in here.. -> blackbox from the library standpoint
* */

public class Id {

    private byte[] idArray = null;

    public Id(byte[] idArray) {
        this.idArray = idArray;
    }

    private Id() {
    }

    public byte[] getIdValue() {
        return idArray;
    }

    @Override
    public String toString() {

        StringBuilder output = new StringBuilder("Id{idValue=");

        for (byte b : idArray) {
            output.append(b);
        }

        output.append("}");

        return output.toString();
    }
}
