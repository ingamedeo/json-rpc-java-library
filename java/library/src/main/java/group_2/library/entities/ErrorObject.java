package group_2.library.entities;

//Error entity, this is included as part of ServerMessages used to report errors
public class ErrorObject {

    public enum ErrorType {
        PARSE_ERROR,
        INVALID_PARAMS,
        INVALID_REQUEST,
        METHOD_NOT_FOUND,
        INTERNAL_ERROR
    }

    private Integer code = null;
    private String message = null;
    private Value data = null;

    public ErrorObject(int code, String message) {
        this.code = code;
        this.message = message;
        this.data = null;
    }

    public ErrorObject(int code, String message, Value structuredData) {
        this.code = code;
        this.message = message;
        this.data = structuredData;
    }

    public static ErrorObject createAsStandardError(ErrorType type, Value structuredData) {
        return new ErrorObject(type, structuredData);
    }

    private ErrorObject(ErrorType type, Value structuredData) {

        switch (type) {
            case PARSE_ERROR:
                code = -32700;
                message = "Parse error";
                data = structuredData;
                break;
            case INVALID_PARAMS:
                code = -32602;
                message = "Invalid params";
                data = structuredData;
                break;
            case INVALID_REQUEST:
                code = -32600;
                message = "Invalid Request";
                data = structuredData;
                break;
            case METHOD_NOT_FOUND:
                code = -32601;
                message = "Method not found";
                data = structuredData;
                break;
            case INTERNAL_ERROR:
                code = -32603;
                message = "Internal error";
                data = structuredData;
                break;
        }

    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Value getData() {
        return data;
    }

}