package group_2.library.entities;

import com.sun.istack.internal.NotNull;

public class ClientMessage {

    //http://www.jsonrpc.org/specification#request_object
    private String method = null;
    private boolean notification;

    //Using Object here, enforcing type below
    private Object id = null;

    //http://www.jsonrpc.org/specification#parameter_structures
    private Value params = null;

    private ClientMessage(ClientMessageBuilder builder) {
        method = builder.method;
        notification = builder.notification;
        id = builder.id;
        params = builder.params;
    }

    public String getMethod() {
        return method;
    }

    public boolean isNotification() {
        return notification;
    }

    public Object getId() {
        return id;
    }

    public Value getParams() {
        return params;
    }



    /*
    *
    * Builder for ClientMessage Objects
    * This ClientMessage is a Request whenever the ID is set
    * Default: Notification
    *
    * */

    public static class ClientMessageBuilder {

        private String method = null;
        private boolean notification = true;

        //Using Object here, enforcing type below
        private Object id = null;

        //http://www.jsonrpc.org/specification#parameter_structures
        private Value params = null;

        public ClientMessageBuilder(@NotNull String method) {
            this.method = method;
        }

        //Note: Letting users set id=null for Request objects. This is discouraged by the specs, but supported.
        public ClientMessageBuilder nullId() {
            this.id = null;
            notification = false;
            return this;
        }

        //Note: Whenever an ID is set, this object becomes a Request. Notifications have no ID as per specs.
        public ClientMessageBuilder id(String id) {
            this.id = id;
            notification = false;
            return this;
        }

        public ClientMessageBuilder id(Number id) {
            this.id = id;
            notification = false;
            return this;
        }

        public ClientMessageBuilder params(Value params) {
            this.params = params;
            return this;
        }

        public ClientMessage build() {
            return new ClientMessage(this);
        }

    }
}
