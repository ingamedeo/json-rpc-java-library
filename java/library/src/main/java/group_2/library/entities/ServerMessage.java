package group_2.library.entities;

/*
*
* As there's much less flexibility in ServerMessages than ClientMessages, no builder is necessary here.
* The constructors listed below allow for ServerMessages with result or error fields
*
* */

public class ServerMessage {

    private Object id = null;
    private Value result = null;
    private ErrorObject error = null;

    public ServerMessage(String id, Value result) {
        this.id = id;
        this.result = result;
        this.error = null;
    }

    public ServerMessage(Number id, Value result) {
        this.id = id;
        this.result = result;
        this.error = null;
    }

    //The constructor copies the id of the clientMessage to avoid ugly casting outside the class. This way, the id is guaranteed to be correct
    public ServerMessage(ClientMessage cm, Value result) {
        this.id = cm.getId();
        this.result = result;
        this.error = null;
    }

    public ServerMessage(String id, ErrorObject error) {
        this.id = id;
        result = null;
        this.error = error;
    }

    public ServerMessage(Number id, ErrorObject error) {
        this.id = id;
        result = null;
        this.error = error;
    }

    public ServerMessage(ClientMessage cm, ErrorObject error) {
        this.id = cm.getId();
        result = null;
        this.error = error;
    }

    public ServerMessage(Value result) {
        this.id = null;
        this.result = result;
        this.error = null;
    }

    public ServerMessage(ErrorObject error) {
        this.id = null;
        result = null;
        this.error = error;
    }

    public Object getId() {
        return id;
    }

    public Value getResult() {
        return result;
    }

    public ErrorObject getError() {
        return error;
    }

}
