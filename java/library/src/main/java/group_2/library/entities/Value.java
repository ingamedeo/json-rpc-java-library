package group_2.library.entities;

import com.google.gson.*;

import java.util.Set;

/*
*
* This class represents a Structured and Primitive type.
* Used whenever both are possible as per JSON-RPC specs. Even though the value is internally stored as JSON,
* this abstracts the JSON-syntax from the client which doesn't know how to format JSON strings, but knows how it wants to structure the field.
*
* */

public class Value {

    //Used to create empty Value(s)
    private enum ValueType {
        MAP,
        ARRAY
    }

    //Using gson library. No Singleton init as per design.
    private static final Gson gson = prepareGson();

    //Object state is a JSON string.
    private String jsonString = null;

    private static Gson prepareGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        return gsonBuilder.create();
    }

    public static Value createAsArrayValue() {
        return new Value(ValueType.ARRAY);
    }

    public static Value createAsMapValue() {
        return new Value(ValueType.MAP);
    }

    //Used to create empty Value(s) instances (Useful to return empty arrays and maps)
    private Value(ValueType types) {
        if(types == ValueType.ARRAY) {
            JsonArray array = new JsonArray();
            jsonString = gson.toJson(array);
        } else if (types == ValueType.MAP) {
            JsonObject object = new JsonObject();
            jsonString = gson.toJson(object);
        }
    }

    //This Value is a JsonPrimitive (String, Long, etc..)
    public Value(Number number) {
        jsonString = gson.toJson(number, Number.class);
    }

    public Value(String string) {
        jsonString = gson.toJson(string, String.class);
    }

    public Value(Boolean b) {
        jsonString = gson.toJson(b, Boolean.class);
    }

    //Constructor calling -> addArrayElement(Value value);
    public Value(Value value) {
        addArrayElement(value);
    }

    //Constructor calling -> add(String key, Value value);
    public Value(String key, Value value) {
        add(key, value);
    }

    public Value () {jsonString = gson.toJson(JsonNull.INSTANCE);}

    //Used internally to get Value objects contained within this Value object
    private Value(JsonElement element) {
        jsonString = gson.toJson(element, JsonElement.class);
    }

    public void add(String key, Value value) {
        JsonElement element = gson.fromJson(jsonString, JsonElement.class);
        JsonObject jsonObject = null;
        if(element != null && element.isJsonObject()) {
            jsonObject = element.getAsJsonObject();
        } else {
            jsonObject = new JsonObject();
        }

        if(value == null) {
            jsonObject.add(key, new Value().getSerialized());
        } else {
            jsonObject.add(key, value.getSerialized());
        }
        jsonString = gson.toJson(jsonObject);
    }

    public void addArrayElement(Value value) {
        JsonElement element = gson.fromJson(jsonString, JsonElement.class);
        JsonArray jsonArray = null;
        if(element != null && element.isJsonArray()) {
            jsonArray = element.getAsJsonArray();
        } else {
            jsonArray = new JsonArray();
        }

        if(value == null) {
            jsonArray.add(new Value().getSerialized());
        } else {
            jsonArray.add(value.getSerialized());
        }
        jsonString = gson.toJson(jsonArray);
    }

    //Used internally, returns the serialized representation of the current Value
    private JsonElement getSerialized() {
        return gson.fromJson(jsonString, JsonElement.class);
    }

    /*
    *
    * The following 3 getters must be called in an OR configuration. They can't be all used on the same Value object.
    *
    * */

    //Returns the primitive value stored in this object
    public Object getPrimitive() {

        if (!isPrimitive()) {
            return null;
        }

        JsonPrimitive primitive = gson.fromJson(jsonString, JsonPrimitive.class);

        if (primitive.isString()) {
            String s = primitive.getAsString();
            return s;
        } else if (primitive.isNumber()) {
            Number n = primitive.getAsNumber();
            return n;
        } else if (primitive.isBoolean()) {
            Boolean b = primitive.getAsBoolean();
            return b;
        }

        return null;
    }

    //Returns the Value (either primitive or structured) stored in this Value (which is an array).
    public Value getArrayElement(int index) {

        if (!isArray()) {
            return null;
        }

        JsonArray jsonArray = gson.fromJson(jsonString, JsonArray.class);

        if (index >= jsonArray.size()) {
            return null;
        }

        JsonElement element = jsonArray.get(index);
        return new Value(element);
    }

    //Returns the Value (either primitive or structured) stored in this Value (which is a Map).
    public Value getMapElement(String key) {

        if (!mapHasKey(key)) {
            return null;
        }

        JsonObject jsonObject = gson.fromJson(jsonString, JsonObject.class);

        JsonElement element = jsonObject.get(key);
        return new Value(element);
    }

    //utility methods to access all array/Map elements
    public Integer getArraySize() {

        if (!isArray()) {
            return null;
        }

        JsonArray jsonArray = gson.fromJson(jsonString, JsonArray.class);
        return jsonArray.size();
    }

    public Set<String> getMapKeySet() {

        if (!isMap()) {
            return null;
        }

        JsonObject jsonObject = gson.fromJson(jsonString, JsonObject.class);
        return jsonObject.keySet();
    }

    public boolean mapHasKey(String key) {

        if (!isMap()) {
            return false;
        }

        JsonObject jsonObject = gson.fromJson(jsonString, JsonObject.class);
        return jsonObject.has(key);
    }

    //End utility methods

    public boolean isPrimitive() {
        JsonElement element = gson.fromJson(jsonString, JsonElement.class);
        return element.isJsonPrimitive();
    }

    public boolean isArray() {
        JsonElement element = gson.fromJson(jsonString, JsonElement.class);
        return element.isJsonArray();
    }

    public boolean isMap() {
        JsonElement element = gson.fromJson(jsonString, JsonElement.class);
        return element.isJsonObject();
    }

    public boolean isNull() {
        JsonElement element = gson.fromJson(jsonString, JsonElement.class);
        return element.isJsonNull();
    }

}