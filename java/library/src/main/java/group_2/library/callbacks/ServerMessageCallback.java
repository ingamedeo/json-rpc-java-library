package group_2.library.callbacks;

/*
*
* Callback interface
*
* The concept of callbacks is to inform a class synchronous / asynchronous if some work in another class is done.
* Some call it the Hollywood principle: "Don't call us we call you".
*
* */

import group_2.library.entities.Id;
import group_2.library.entities.ServerMessage;

public interface ServerMessageCallback {
    void onServerMessageReceived(ServerMessage serverMessage, Id id, String ip, int port);
    void onServerBatchMessageReceived(ServerMessage[] serverBatchMessage, Id id, String ip, int port);
}
