package group_2.library.callbacks;

import group_2.library.entities.Id;

public interface JRPCMessageCallback {
    void onJRPCMessageReceived(String ip, int port, Id id, String message);

}
