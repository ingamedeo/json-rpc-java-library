package group_2.library;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.MalformedJsonException;
import group_2.library.entities.ClientMessage;
import group_2.library.entities.ErrorObject;
import group_2.library.entities.ServerMessage;
import group_2.library.entities.Value;
import group_2.library.gsoncustom.ClientMessageAdapter;
import group_2.library.gsoncustom.ErrorAdapter;
import group_2.library.gsoncustom.ServerMessageAdapter;
import group_2.library.gsoncustom.ValueAdapter;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import static com.google.gson.stream.JsonToken.END_DOCUMENT;

public class Utils {

    public static Gson prepareGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        gsonBuilder.registerTypeAdapter(ClientMessage.class, new ClientMessageAdapter());
        gsonBuilder.registerTypeAdapter(ServerMessage.class, new ServerMessageAdapter());
        gsonBuilder.registerTypeAdapter(ErrorObject.class, new ErrorAdapter());
        gsonBuilder.registerTypeAdapter(Value.class, new ValueAdapter());
        return gsonBuilder.create();
    }

    //Invoke this method to catch ParseErrors
    public static boolean isValidJSON(final String json) {
        return isValidJSON(new StringReader(json));
    }

    private static boolean isValidJSON(final Reader reader)  {
        return isValidJSON(new JsonReader(reader));
    }

    private static boolean isValidJSON(final JsonReader jsonReader) {
        try {
            JsonToken token;
            while ( (token = jsonReader.peek()) != END_DOCUMENT && token != null ) {
                switch ( token ) {
                    case BEGIN_ARRAY:
                        jsonReader.beginArray();
                        break;
                    case END_ARRAY:
                        jsonReader.endArray();
                        break;
                    case BEGIN_OBJECT:
                        jsonReader.beginObject();
                        break;
                    case END_OBJECT:
                        jsonReader.endObject();
                        break;
                    case NAME:
                        jsonReader.nextName();
                        break;
                    case STRING:
                    case NUMBER:
                    case BOOLEAN:
                    case NULL:
                        jsonReader.skipValue();
                        break;
                    case END_DOCUMENT:
                        return isValidJSON(jsonReader);
                    default:
                        throw new AssertionError(token);
                }
            }
            return true;
        } catch ( final MalformedJsonException e ) {
            return false;
        } catch (IOException e) {
            //We weren't able to parse this. We assume this isn't valid json.
            return false;
        }
    }

    public static boolean isAllNullArray(Object[] array) {

        for (Object o : array) {
            if (o!=null) {
                return false;
            }
        }

        return true;
    }

    public static void stripNullObjFromList(List objects) {
        //Don't send null messages to client
        for (int i=0; i<objects.size(); i++) {
            if (objects.get(i)==null) {
                objects.remove(i);
            }
        }
    }
}
