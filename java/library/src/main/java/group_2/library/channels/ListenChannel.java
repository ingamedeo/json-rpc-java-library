package group_2.library.channels;

import group_2.library.Pair;
import group_2.library.entities.Id;

public interface ListenChannel {
    void bind(String ip, int port);
    void close();
    Pair<Id, String> receive();
    boolean send(Id id, String message);
}
