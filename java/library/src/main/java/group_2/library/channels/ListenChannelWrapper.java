package group_2.library.channels;

import com.sun.istack.internal.NotNull;
import group_2.library.Pair;
import group_2.library.callbacks.JRPCMessageCallback;
import group_2.library.entities.Id;
import group_2.library.managers.ReceiveManager;

public class ListenChannelWrapper {

    private String ip = null;
    private Integer port = null;
    private ListenChannel listenChannel = null;
    private Thread thread = null;

    private JRPCMessageCallback callback = null;

    public ListenChannelWrapper(@NotNull String ip, int port, @NotNull ListenChannel listenChannel) {
        this.ip = ip;
        this.port = port;
        this.listenChannel = listenChannel;

        //set class we should be calling callbacks on
        callback = (JRPCMessageCallback) ReceiveManager.getInstance();

        //Initialise underlying channel by binding to specified ip and port
        initChannel();

        thread = new Thread(new ListenRunnable());
        thread.start();
    }

    private void initChannel() {
        listenChannel.bind(ip, port);
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public boolean send(Id id, String message) {
        System.out.println("ListenChannelWrapper << Message ready: " + message);
        return listenChannel.send(id, message);
    }

    public void close() {
        thread.interrupt();
    }

    private class ListenRunnable implements Runnable {
        public void run() {

            System.out.println("Starting ListenChannel (" + ip + ", " + port + ") ..");

            while (!Thread.currentThread().isInterrupted()) {

                /*
                *
                * receive() is assumed *blocking*, the underlying channel should implement a Queue to avoid message loss.
                * receive() may not be called continuously by the library. Best-effort, messages will be delivered to the client
                * in the shortest time possible.
                *
                * */

                Pair<Id, String> messagePair = listenChannel.receive();


                Id id = messagePair.getFirst();
                String message = messagePair.getSecond();
                if (message==null) {
                    continue;
                }

                System.out.println("ListenChannelWrapper << Message received: " + message + " with id: " + id);
                //Pass received message to ReceiveManager for further handling
                callback.onJRPCMessageReceived(ip, port, id, message);
            }

            listenChannel.close();
        }
    }
}
