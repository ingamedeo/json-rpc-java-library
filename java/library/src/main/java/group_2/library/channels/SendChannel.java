package group_2.library.channels;

public interface SendChannel {

    void connect(String ip, int port);
    boolean send(String message);
    String receive();
    void close();

}
