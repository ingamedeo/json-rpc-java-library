package group_2.library.channels;

import com.sun.istack.internal.NotNull;
import group_2.library.callbacks.JRPCMessageCallback;
import group_2.library.managers.ReceiveManager;

public class SendChannelWrapper {

    private String serverIp = null;
    private Integer serverPort = null;
    private SendChannel sendChannel = null;
    private Thread thread = null;

    private JRPCMessageCallback callback = null;

    public SendChannelWrapper(@NotNull String serverIp, int serverPort, @NotNull SendChannel sendChannel) {
        this.sendChannel = sendChannel;
        this.serverIp = serverIp;
        this.serverPort = serverPort;

        //set class we should be calling callbacks on
        callback = (JRPCMessageCallback) ReceiveManager.getInstance();

        //Initialise underlying channel by binding to specified ip and port
        initChannel();

        thread = new Thread(new SendRunnable());
        thread.start();
    }

    private void initChannel() {
        sendChannel.connect(serverIp, serverPort);
    }

    public String getServerIp() {
        return serverIp;
    }

    public int getServerPort() {
        return serverPort;
    }

    public boolean send(String message) {
        return sendChannel.send(message);
    }

    public void close() {
        thread.interrupt();
    }

    private class SendRunnable implements Runnable {

        public void run() {

            System.out.println("Starting SendChannel (serverIp: "+serverIp+", serverPort: "+ serverPort+") ..");

            while (!Thread.currentThread().isInterrupted()) {

                String message = sendChannel.receive();

                if (message==null) {
                    continue;
                }

                System.out.println("SendChannelWrapper << Message received: " + message);

                //Pass received message to ReceiveManager for further handling
                callback.onJRPCMessageReceived(serverIp, serverPort, null, message);
            }

            sendChannel.close();
        }
    }
}
