package group_2.library.channels;

public abstract class AbstractChannelFactory {
    public abstract ListenChannel createListenChannel(); //Signature unknown at this stage?
    public abstract SendChannel createSendChannel();
}
