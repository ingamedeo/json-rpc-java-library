package group_2.library.managers;

import group_2.library.entities.ClientMessage;
import group_2.library.entities.Id;

import java.util.HashMap;
import java.util.Map;

/*
 *
 * This class provides methods for creating and handling single messages.
 * Pending messages are stored here, until the corresponding ServerMessage is received from the client.
 *
 * */

class PendingSingleManager {

    //Class holding ip, port and id, later used in a Map to link these details with the message itself.
    private static class ClientMessageDetails {
        private String ip = null;
        private Integer port = null;
        private Id id = null;

        ClientMessageDetails(String ip, int port, Id id) {
            this.ip = ip;
            this.port = port;
            this.id = id;
        }

        private String getIp() {
            return ip;
        }

        private Integer getPort() {
            return port;
        }

        private Id getId() {
            return id;
        }
    }

    private static PendingSingleManager instance = null;

    private Map<ClientMessage, ClientMessageDetails> pendingMap = new HashMap<ClientMessage, ClientMessageDetails>();


    static synchronized PendingSingleManager getInstance() {
        if(instance == null) {
            instance = new PendingSingleManager();
        }
        return instance;
    }

    //Called by ReceiveManager
    void addMessage(String ip, int port, Id id, ClientMessage clientMessage) {
        ClientMessageDetails pending = new ClientMessageDetails(ip, port, id);
        pendingMap.put(clientMessage, pending);
    }

    boolean isPending(ClientMessage clientMessage) {
        ClientMessageDetails temp = pendingMap.get(clientMessage);
        return temp != null;
    }

    String getIpFromClientMessage(ClientMessage clientMessage) {
        ClientMessageDetails temp = pendingMap.get(clientMessage);
        if (temp != null){
            return temp.getIp();
        }
        return null;
    }

    Integer getPortFromClientMessage(ClientMessage clientMessage) {
        ClientMessageDetails temp = pendingMap.get(clientMessage);
        if (temp != null){
            return temp.getPort();
        }
        return null;
    }

    Id getIdFromClientMessage(ClientMessage clientMessage) {
        ClientMessageDetails temp = pendingMap.get(clientMessage);
        if (temp != null){
            return temp.getId();
        }
        return null;
    }

    void removeMessage(ClientMessage clientMessage) {
        pendingMap.remove(clientMessage);
    }
}
