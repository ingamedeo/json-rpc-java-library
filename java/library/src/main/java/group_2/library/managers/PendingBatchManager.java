package group_2.library.managers;

import com.sun.istack.internal.NotNull;
import group_2.library.entities.ClientMessage;
import group_2.library.entities.ErrorObject;
import group_2.library.entities.Id;
import group_2.library.entities.ServerMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
*
* This class provides methods for creating and filling batch messages.
* Batch server messages are only sent once all ServerMessages belonging to that batch message are ready and have been processed/created by the client,
* until then they are held here.
*
* */

class PendingBatchManager {

    /*
    *
    * Stores the batch message status, keeping track of how many server messages have been already stored and how many are still missing.
    * It also pairs the Batch Message with the IP, port on which it has to be sent.
    *
    * */

    private static class BatchMessageDetails {
        private static Integer nextId = 0;
        private Integer totalSpots = null;
        private String ip = null;
        private Integer port = null;
        private Id id = null;
        private List<ServerMessage> serverMessageList = null;

        BatchMessageDetails(int totalSpots, String ip, int port, Id id) {
            this.totalSpots = totalSpots;
            this.ip = ip;
            this.port = port;
            this.id = id;
            this.serverMessageList = new ArrayList<>();
        }

        void addServerMessage(ServerMessage serverMessage) {
           if (serverMessageList.size() < totalSpots){
               serverMessageList.add(serverMessage);
           }
        }

        private String getIp() {
            return ip;
        }

        private Integer getPort() {
            return port;
        }

        private Id getId() {
            return id;
        }

        Integer getRemainingSpots() {
            return totalSpots - serverMessageList.size();
        }

        List<ServerMessage> getServerMessageList() {
            return serverMessageList;
        }

        static Integer getNextId() {
            nextId++;
            return nextId;
        }
    }
    //End inner static

    private static PendingBatchManager instance = null;
    private HashMap<ClientMessage, Integer> pendingMap = null;
    private HashMap<Integer, BatchMessageDetails> batchRequestMap = null;

    private PendingBatchManager() {
        batchRequestMap = new HashMap<>();
        pendingMap = new HashMap<>();
    }

    static synchronized PendingBatchManager getInstance() {
        if(instance == null) {
            instance = new PendingBatchManager();
        }
        return instance;
    }

    //Create a new pending batch message
    synchronized void createReceivedBatchMessage(@NotNull String ip, int port, Id id, @NotNull ClientMessage[] clientMessages) {

        //Assign a numeric id
        Integer batchId = BatchMessageDetails.getNextId();

        //How many messages should I reply to?
        int count = 0;
        int errorCount = 0;
        for (ClientMessage clientMessage : clientMessages) {
            if(clientMessage == null) {
                errorCount++;
            } else if (!clientMessage.isNotification()) {
                pendingMap.put(clientMessage, batchId);
                count++;
            }
        }

        if(count + errorCount > 0) { //Else the server should not respond
            BatchMessageDetails batchMessageDetails = new BatchMessageDetails(count + errorCount, ip, port, id);
            batchRequestMap.put(batchId, batchMessageDetails);

            ErrorObject invalidError = ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_REQUEST, null);
            ServerMessage serverErrorMessage = new ServerMessage(invalidError);
            for(int i = 0; i < errorCount; i++) {
                batchMessageDetails.addServerMessage(serverErrorMessage);
            }
        }
    }

    Integer addServerMessageToBatch(ClientMessage clientMessage, ServerMessage serverMessage) {
        //Check if the message belongs to that batch
        Integer batchId = pendingMap.get(clientMessage);
        if(batchId == null) {
            return null;
        }
        //Save the message into the batch
        BatchMessageDetails batchMessageDetails = batchRequestMap.get(batchId); //null if not found
        batchMessageDetails.addServerMessage(serverMessage);

        //Remove the message form the map
        pendingMap.remove(clientMessage);

        //Return the batchId
        return batchId;
    }

    String getIpFromBatchId(int batchId) {
        BatchMessageDetails temp = batchRequestMap.get(batchId);
        if (temp != null){
            return temp.getIp();
        }
        return null;
    }

    Integer getPortFromBatchId(int batchId) {
        BatchMessageDetails temp = batchRequestMap.get(batchId);
        if (temp != null){
            return temp.getPort();
        }
        return null;
    }

    Id getIdFromBatchId(int batchId) {
        BatchMessageDetails temp = batchRequestMap.get(batchId);
        if (temp != null){
            return temp.getId();
        }
        return null;
    }

    void removeMessage(int batchId) {
        batchRequestMap.remove(batchId);
    }

    //Called by SendManager when the checkBatch method returns 0, returns the arraylist of messages ready to send
    List<ServerMessage> getBatchServerMessages (Integer batchId) {
        BatchMessageDetails batchMessageDetails = batchRequestMap.get(batchId);
        if (batchMessageDetails == null) {
            return null;
        }
        if (batchMessageDetails.getRemainingSpots() == 0){
            return batchMessageDetails.getServerMessageList();
        }
        return null;
    }
}
