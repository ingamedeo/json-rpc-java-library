package group_2.library.managers;

import com.sun.istack.internal.NotNull;
import group_2.library.channels.*;

import java.util.ArrayList;
import java.util.List;

/*
*
* This manager handles Channels, they can be of two types as their network role differs.
* It provides methods for creating new channels and managing existing ones.
*
* */

public class ChannelManager {

    private static final List<ListenChannelWrapper> listenChannelList = new ArrayList<>();
    private static final List<SendChannelWrapper> sendChannelList = new ArrayList<>();

    private static ChannelManager mInstance = null;

    private AbstractChannelFactory channelFactory = null;

    private ChannelManager() {
    }

    /* Only one instance can be alive */
    public static synchronized ChannelManager getInstance() {
        if (mInstance == null) {
            mInstance = new ChannelManager();
        }
        return mInstance;
    }

    public void setChannelFactory(AbstractChannelFactory channelFactory) {
        this.channelFactory = channelFactory;
    }

    private AbstractChannelFactory getChannelFactory() {

        if (channelFactory==null) {
            throw new IllegalStateException("Library was not initialized. Call Library.init()");
        }

        return channelFactory;
    }

    public void startListenChannel(@NotNull String ip, int port) {
        ListenChannelWrapper listenChannelWrapper = new ListenChannelWrapper(ip, port, getChannelFactory().createListenChannel());
        listenChannelList.add(listenChannelWrapper);
    }

    private SendChannelWrapper startSendChannel(@NotNull String serverIp, int serverPort) {
        SendChannel sendChannel = getChannelFactory().createSendChannel();
        SendChannelWrapper sendChannelWrapper = new SendChannelWrapper(serverIp, serverPort, sendChannel);
        sendChannelList.add(sendChannelWrapper);
        return sendChannelWrapper;
    }

    ListenChannelWrapper getListenChannelByIpAndPort(@NotNull String ip, int port) {

        for (ListenChannelWrapper channelWrapper : listenChannelList) {
            if (channelWrapper.getIp().equals(ip) && channelWrapper.getPort()==port) {
                return channelWrapper;
            }
        }

        return null;
    }

    SendChannelWrapper getSendChannelByServerIpAndPort(@NotNull String ip, int port) {

        for (SendChannelWrapper channelWrapper : sendChannelList) {
            if (channelWrapper.getServerIp().equals(ip) && channelWrapper.getServerPort()==port) {
                return channelWrapper;
            }
        }

        return startSendChannel(ip, port);
    }

    public synchronized void stopAllChannels() {
        for (ListenChannelWrapper listenChannelWrapper : listenChannelList) {
            listenChannelWrapper.close();
        }

        listenChannelList.clear();

        for (SendChannelWrapper sendChannelWrapper : sendChannelList) {
            sendChannelWrapper.close();
        }

        sendChannelList.clear();
    }
}
