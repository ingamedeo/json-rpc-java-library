package group_2.library.managers;

import com.google.gson.JsonSyntaxException;
import group_2.library.Utils;
import group_2.library.callbacks.ClientMessageCallback;
import group_2.library.callbacks.JRPCMessageCallback;
import group_2.library.callbacks.ServerMessageCallback;
import group_2.library.entities.ClientMessage;
import group_2.library.entities.ErrorObject;
import group_2.library.entities.Id;
import group_2.library.entities.ServerMessage;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

//Class responsible for receiving messages from the network and delivering them to the library client.
public class ReceiveManager implements JRPCMessageCallback {

    private static ReceiveManager mInstance = null;

    /*
     Suppose that the garbage collector determines at a certain point in time that an object is weakly reachable.
     At that time it will atomically clear all weak references to that object and all weak references to any other
     weakly-reachable objects from which that object is reachable through a chain of strong and soft references.
     */

    private WeakReference<ClientMessageCallback> clientMessageCallbackRef = null;
    private WeakReference<ServerMessageCallback> serverMessageCallbackRef = null;

    private ReceiveManager() {
    }

    // Only one instance can be alive
    public static synchronized ReceiveManager getInstance() {
        if (mInstance == null) {
            mInstance = new ReceiveManager();
        }
        return mInstance;
    }

    //Set user callback, messages are pushed to the user once the library is done with them
    public void setClientMessageCallback(ClientMessageCallback clientMessageCallback) {
        this.clientMessageCallbackRef = new WeakReference<ClientMessageCallback>(clientMessageCallback);
    }

    public void setServerMessageCallback(ServerMessageCallback serverMessageCallback) {
        this.serverMessageCallbackRef = new WeakReference<>(serverMessageCallback);
    }

    //Receives JSON-string messages from the network layer, parses them and sends them to the right callback for delivery to the client application
    @Override
    public synchronized void onJRPCMessageReceived(String ip, int port, Id id, String message) {

        ClientMessage clientMessage = null;
        ServerMessage serverMessage = null;
        boolean singleParseFailed = false;

        if (!Utils.isValidJSON(message)) {
            ErrorObject parseError = ErrorObject.createAsStandardError(ErrorObject.ErrorType.PARSE_ERROR, null);
            ServerMessage serverErrorMessage = new ServerMessage(parseError);
            SendManager.getInstance().sendSingleError(serverErrorMessage, ip, port, id);
            return;
        }
        //WE ARE 100% SURE THIS IS VALID JSON AT THIS STAGE!

        //First try to parse the message as a single message
        try {
            //Parse as ClientMessage
            clientMessage = Utils.prepareGson().fromJson(message, ClientMessage.class);
            //Parse as ServerMessage
            serverMessage = Utils.prepareGson().fromJson(message, ServerMessage.class);
        } catch (JsonSyntaxException e) {

            System.out.println("JsonSyntaxException thrown while parsing single requests.");

            //message is NOT a Json Object, it may be a Json Array.
            singleParseFailed = true;
        }

        //If the single parse was successful, then process the single message and return
        if(!singleParseFailed) {
            sendJRPCMessageToCallbackOrError(ip, port, id, clientMessage, serverMessage);
            return;
        }

        //Note that the parse procedure creates empty arrays if unsuccessful (interprets the messages as invalid)
        ClientMessage[] clientBatchMessage = null;
        ServerMessage[] serverBatchMessage = null;

        try {
            //Parse as ClientMessage batch
            clientBatchMessage = Utils.prepareGson().fromJson(message, ClientMessage[].class);
            //Parse as ServerMessage batch
            serverBatchMessage = Utils.prepareGson().fromJson(message, ServerMessage[].class);
        } catch (JsonSyntaxException e) {

            ErrorObject invalidError = ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_REQUEST, null);
            ServerMessage serverErrorMessage = new ServerMessage(invalidError);
            SendManager.getInstance().sendSingleError(serverErrorMessage, ip, port, id);

            return;
        }

        //Strip null messages, send to relevant callback or error
        sendJRPCBatchMessageToCallbackOrError(ip, port, id, clientBatchMessage, serverBatchMessage);
    }

    //Handle single messages, deliver them to the relevant callback
    private void sendJRPCMessageToCallbackOrError(String ip, int port, Id id, ClientMessage clientMessage, ServerMessage serverMessage) {

        //Normal message handling
        if (clientMessage!=null && serverMessage==null) {
            if (!clientMessage.isNotification()){
                PendingSingleManager.getInstance().addMessage(ip, port, id, clientMessage);
            }

            //Everything went well! Deliver message to the user
            if (clientMessageCallbackRef!=null) {
                ClientMessageCallback clientMessageCallback = clientMessageCallbackRef.get();
                if (clientMessageCallback!=null) { //Being extra cautious the reference is still there!
                    clientMessageCallback.onClientMessageReceived(clientMessage, id, ip, port);
                }
            }

        } else if (clientMessage==null && serverMessage!=null) {

            //Everything went well! Deliver message to the user
            if (serverMessageCallbackRef!=null) {
                ServerMessageCallback serverMessageCallback = serverMessageCallbackRef.get();
                if (serverMessageCallback!=null) { //Being extra cautious the reference is still there!
                    serverMessageCallback.onServerMessageReceived(serverMessage, id, ip, port);
                }
            }

        } else { //We were unable to receive a ClientMessage or a ServerMessage, but we know this is VALID JSON.

            //Invalid request
            ErrorObject invalidError = ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_REQUEST, null);
            ServerMessage serverErrorMessage = new ServerMessage(invalidError);
            SendManager.getInstance().sendSingleError(serverErrorMessage, ip, port, id);
        }
    }

    //Handle batch messages, deliver them to the relevant callback
    private void sendJRPCBatchMessageToCallbackOrError(String ip, int port, Id id, ClientMessage[] clientBatchMessage, ServerMessage[] serverBatchMessage) {

        //Converts array to LinkedList
        List<ClientMessage> clientMessageList = new LinkedList<>();
        List<ServerMessage> serverMessageList = new LinkedList<>();
        clientMessageList.addAll(Arrays.asList(clientBatchMessage));
        serverMessageList.addAll(Arrays.asList(serverBatchMessage));

        if (clientMessageList.size() != serverMessageList.size()) {
            ErrorObject invalidError = ErrorObject.createAsStandardError(ErrorObject.ErrorType.INTERNAL_ERROR, null);
            ServerMessage serverErrorMessage = new ServerMessage(invalidError);
            SendManager.getInstance().sendSingleError(serverErrorMessage, ip, port, id);
            return;
        }

        boolean isClient = !Utils.isAllNullArray(clientBatchMessage);
        boolean isServer = !Utils.isAllNullArray(serverBatchMessage);

        //Process the batch message
        if (isClient && !isServer) {
            //Batch message found! Add the messages to PendingBatchManager
            //The error responses are added directly by the manager for data consistency reasons

            PendingBatchManager.getInstance().createReceivedBatchMessage(ip, port , id, clientBatchMessage);

            Utils.stripNullObjFromList(clientMessageList);

            if (clientMessageCallbackRef!=null) {
                ClientMessageCallback clientMessageCallback = clientMessageCallbackRef.get();
                if (clientMessageCallback != null) { //Being extra cautious the reference is still there!
                    clientMessageCallback.onClientBatchMessageReceived(clientMessageList.toArray(new ClientMessage[clientMessageList.size()]), id, ip, port);
                }
            }

        } else if (!isClient && isServer) {
            //Deliver the not-null messages to the user

            Utils.stripNullObjFromList(serverMessageList);

            if (serverMessageCallbackRef!=null) {
                ServerMessageCallback serverMessageCallback = serverMessageCallbackRef.get();
                if (serverMessageCallback!=null) { //Being extra cautious the reference is still there!
                    serverMessageCallback.onServerBatchMessageReceived(serverMessageList.toArray(new ServerMessage[serverMessageList.size()]), id, ip, port);
                }
            }

        } else {
            //Invalid request
            ErrorObject invalidError = ErrorObject.createAsStandardError(ErrorObject.ErrorType.INVALID_REQUEST, null);
            ServerMessage serverErrorMessage = new ServerMessage(invalidError);
            SendManager.getInstance().sendSingleError(serverErrorMessage, ip, port, id);
        }
    }
}