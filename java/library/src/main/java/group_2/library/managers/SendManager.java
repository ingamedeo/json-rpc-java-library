package group_2.library.managers;

import com.sun.istack.internal.NotNull;
import group_2.library.Utils;
import group_2.library.channels.ListenChannelWrapper;
import group_2.library.channels.SendChannelWrapper;
import group_2.library.entities.ClientMessage;
import group_2.library.entities.Id;
import group_2.library.entities.ServerMessage;

import java.util.List;

//Class responsible for sending messages (both single and batch) from the client and delivering them via the network channels.
public class SendManager {

    private static SendManager instance = null;

    private SendManager() {
    }

    public static synchronized SendManager getInstance() {
        if(instance == null) {
            instance = new SendManager();
        }
        return instance;
    }

    private boolean sendMessage(@NotNull String jrpcMessage, @NotNull String listenIp, int listenPort, Id id) {
            ListenChannelWrapper listenChannelWrapper = ChannelManager.getInstance().getListenChannelByIpAndPort(listenIp, listenPort);
            if (listenChannelWrapper != null && id != null) {
                return listenChannelWrapper.send(id, jrpcMessage);
            }else {
                SendChannelWrapper sendChannelWrapper = ChannelManager.getInstance().getSendChannelByServerIpAndPort(listenIp, listenPort);
                return sendChannelWrapper.send(jrpcMessage);
            }
    }

    void sendSingleError(@NotNull ServerMessage serverMessage, @NotNull String listenIp, int listenPort, Id id) {

        if (serverMessage != null && serverMessage.getError()!=null) {
            String jrpcServerMessage = Utils.prepareGson().toJson(serverMessage, ServerMessage.class);
            sendMessage(jrpcServerMessage, listenIp, listenPort, id);
        }
    }

    //A ListenChannel might be selected and the identity given is invalid, This only happens when using both client/server on the same host
    public boolean sendClientMessage(ClientMessage clientMessage, String listenIp, int listenPort, Id id) {

        if(clientMessage != null) {
            String message = Utils.prepareGson().toJson(clientMessage, ClientMessage.class);
            return sendMessage(message, listenIp, listenPort, id);
        }
        return false;
    }

    public boolean sendBatchClientMessage(ClientMessage[] batchMessage, String listenIp, int listenPort, Id id) {
        if(batchMessage != null) {

            for (ClientMessage clientMessage : batchMessage) {
                if (clientMessage==null) {
                    return false;
                }
            }

            String message = Utils.prepareGson().toJson(batchMessage, ClientMessage[].class);
            return sendMessage(message, listenIp, listenPort, id);
        }
        return false;
    }

    public boolean respond(ServerMessage serverMessage, ClientMessage clientMessage, boolean batch) {

        if(serverMessage == null || clientMessage == null) {
            return false;
        }
        //First of all check if the message is a single response/error
        if (!batch) {
            if (PendingSingleManager.getInstance().isPending(clientMessage)) {
                String ip = PendingSingleManager.getInstance().getIpFromClientMessage(clientMessage);
                Integer port = PendingSingleManager.getInstance().getPortFromClientMessage(clientMessage);
                Id id = PendingSingleManager.getInstance().getIdFromClientMessage(clientMessage);

                    String message = Utils.prepareGson().toJson(serverMessage, ServerMessage.class);
                    boolean sent = sendMessage(message, ip, port, id);
                    if (sent) {
                        PendingSingleManager.getInstance().removeMessage(clientMessage);
                        return true;
                    }

                return false;
            }
        } else {
            Integer batchId = PendingBatchManager.getInstance().addServerMessageToBatch(clientMessage, serverMessage);
            if (batchId != null) {
                List<ServerMessage> messagesToSend = PendingBatchManager.getInstance().getBatchServerMessages(batchId);
                if (messagesToSend != null){
                    String ip = PendingBatchManager.getInstance().getIpFromBatchId(batchId);
                    Integer port = PendingBatchManager.getInstance().getPortFromBatchId(batchId);
                    Id id = PendingBatchManager.getInstance().getIdFromBatchId(batchId);
                    ServerMessage[] serverMessageArray = messagesToSend.toArray(new ServerMessage[messagesToSend.size()]);
                    String batchServerMessage = Utils.prepareGson().toJson(serverMessageArray, ServerMessage[].class );
                    boolean sent = sendMessage(batchServerMessage, ip, port, id);
                    if(sent) {
                        PendingBatchManager.getInstance().removeMessage(batchId);
                        return true;
                    }
                    return false;
                }
            }
        }

        return false;
    }

}
